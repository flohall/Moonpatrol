package main;

import java.awt.Graphics2D;

/**
 * Write a description of class UfoTyp here.
 *
 * @author Florian Hallay
 * @version 02.05.2012
 */
public class UfoTyp extends NullTyp {

    private final Ufo ufo;

    public UfoTyp(final Auto pAuto) {
        super(pAuto);
        ufo = new Ufo(pAuto);
    }

    @Override
    public void zeichne(final Graphics2D g) {
        ufo.zeichne(g);
    }

    @Override
    public void berechne(final double faktor) {
        if (faktor != 0)
            bewege(faktor);
        ufo.berechne(faktor);
    }

    @Override
    protected void bewege(final double faktor) {
        final double speed = Werte.hindernisSpeed() * faktor;
        pos.x -= speed;
    }

    @Override
    protected boolean isCloseable() {
        return ufo.isCloseable();
    }

    @Override
    protected void setCloseable() {
        ufo.setCloseable();
    }

}
