package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.ArrayList;

/**
 * Class Spielverwaltung
 * Klasse zum Verwalten der spielspezifischen Ablaeufe
 *
 * @author Florian Hallay
 * @version 01.06.2013
 */
public class Spielverwaltung {

    // Objekte
    public final Auto car;

    private final Hintergrund hintergrund;

    private final Typverwaltung typen;

    private boolean neuesSpiel;

    private boolean fortsetzen;

    private boolean gameOver;

    private boolean bummFertig;

    private final MainPanel mPanel;

    // Konstruktor
    public Spielverwaltung(final MainPanel panel) {
        car = new Auto();
        hintergrund = new Hintergrund();
        typen = new Typverwaltung();
        mPanel = panel;
    }

    // Dienste
    private void neu() {
        hintergrund.reset();
        car.reset();
        typen.reset();
        gameOver = false;
        neuesSpiel = false;
        fortsetzen = false;
        bummFertig = false;
    }

    public int nummer() {
        return typen.nummer();
    }

    /**
     * ZEICHNEN
     */
    public void zeichneUntereEbene(final Graphics2D g) {
        hintergrund.zeichneDrunter(g);
    }

    /**
     * ZEICHNEN
     */
    public void zeichneObereEbene(final Graphics2D g) {
        hintergrund.zeichneDrueber(g);
        typen.zeichneUnterAuto(g);
        car.zeichne(g);
        typen.zeichneUeberAuto(g);
    }

    private void berechneKomponenten(final double faktor) {
        hintergrund.berechne(faktor);
        car.berechne(faktor);
        typen.berechne(faktor);
    }

    /**
     * BERECHNEN
     */
    public void berechne(final double faktor) {
        if (gameOver) {
            if (fortsetzen) {
                neu();
                Werte.setzeVollesLeben();
                Werte.setzeNullPunkte();
            }
            if (neuesSpiel) {
                komplettReset();
            }
        } else if (!car.gibGetroffen()) {
            berechneKomponenten(faktor);
        } else {
            berechneKomponenten(0);
            car.explosion.bearbeite(faktor);
            if (!car.explosion.istAktiv()) {
                if (Werte.istTot()) //wenn Leben == 0
                {
                    mPanel.highscoreAbfrage();
                    gameOver = true;
                } else
                    neu();
            }
        }
    }

    public void autoSollSchiessen() {
        car.setzeSollFeuern(true);
    }

    public void autoSollSpringen() {
        car.setzeSollSpringen(true);
    }

    public void beschleunige(final boolean flag) {
        car.beschleunige(flag);
    }

    public void bremse(final boolean flag) {
        car.bremse(flag);
    }

    public void continueGame() {
        fortsetzen = true;
    }

    public void newGame() {
        neuesSpiel = true;
    }

    public void komplettReset() {
        neu();
        Werte.setzeVollesLeben();
        Werte.setzeNullPunkte();
        typen.checkpointreset();
    }

    public class Typverwaltung {

        private final ArrayList<Typ> aktionsListe;

        private int bonusPointsKassiert;

        //Attribute
        private int reihenfolge;

        //final Attribute
        private static final int NIX = 0;

        private static final int CHECKPOINT = 1;

        private static final int HAUFENNORMAL = 2;

        private static final int HAUFENKLEIN = 3;

        private static final int HAUFENGROSS = 4;

        private static final int LOCHNORMAL = 5;

        private static final int LOCHGROSS = 6;

        private static final int MINE = 7;

        private static final int UFO = 8;

        /*
         * {x,y} levelDesign[hindernisNummer-1][0]=x , levelDesign[hindernisNummer-1][1]=y
         * => x=hindernisCode siehe oben //gibt Hindernis Art an
         * => y=abstand 0<=y<100  //Abstand zum Vorgaenger Hindernis //y*10 Abstand in Pixel
         */
        private final int[][] levelDesign = {
                {5, 90}, {5, 30}, {2, 50}, {8, 40}, {8, 50}, {0, 50}, {8, 90}, {0, 90}, {2, 40}, {1, 20}, //Bis A
                {7, 30}, {7, 40}, {7, 20}, {7, 05}, {7, 16}, {7, 40}, {7, 01}, {7, 40}, {7, 15}, {1, 20}, //Bis B
                {2, 40}, {8, 30}, {5, 20}, {3, 70}, {5, 40}, {6, 50}, {5, 60}, {2, 30}, {5, 60}, {1, 20}, //Bis C
                {5, 40}, {2, 60}, {5, 30}, {4, 60}, {5, 40}, {5, 70}, {2, 30}, {5, 40}, {2, 60}, {1, 20}, //Bis D
                {8, 50}, {8, 10}, {8, 30}, {0, 80}, {5, 30}, {6, 20}, {3, 40}, {5, 30}, {2, 50}, {1, 20}, //Bis E

                {2, 30}, {2, 40}, {7, 50}, {7, 30}, {7, 01}, {7, 01}, {7, 40}, {7, 20}, {7, 05}, {1, 20}, //Bis F
                {7, 30}, {7, 50}, {7, 20}, {7, 05}, {7, 30}, {7, 16}, {7, 01}, {7, 25}, {7, 32}, {1, 20}, //Bis G
                {8, 99}, {8, 05}, {8, 15}, {8, 20}, {8, 10}, {8, 30}, {0, 90}, {0, 90}, {0, 90}, {1, 20}, //Bis H
                {2, 50}, {5, 50}, {5, 20}, {2, 30}, {2, 30}, {2, 20}, {6, 50}, {5, 25}, {4, 30}, {1, 20}, //Bis I
                {2, 30}, {5, 20}, {5, 20}, {2, 10}, {5, 10}, {3, 30}, {8, 50}, {4, 20}, {4, 20}, {1, 20}, //Bis J

                {8, 60}, {5, 30}, {5, 20}, {6, 40}, {2, 50}, {2, 30}, {2, 00}, {5, 30}, {5, 30}, {1, 20}, //Bis K
                {2, 30}, {2, 30}, {6, 50}, {4, 30}, {3, 20}, {3, 20}, {5, 30}, {3, 40}, {5, 50}, {1, 20}, //Bis L
                {5, 10}, {6, 80}, {5, 20}, {5, 20}, {5, 20}, {8, 10}, {2, 60}, {2, 40}, {2, 40}, {1, 20}, //Bis M
                {2, 50}, {5, 50}, {5, 20}, {8, 80}, {2, 00}, {2, 20}, {5, 50}, {5, 20}, {4, 30}, {1, 20}, //Bis N
                {5, 50}, {5, 50}, {2, 40}, {2, 30}, {6, 90}, {6, 40}, {2, 50}, {2, 20}, {2, 50}, {1, 20}, //Bis O

                {5, 30}, {5, 30}, {5, 30}, {2, 50}, {2, 50}, {6, 40}, {3, 50}, {4, 30}, {7, 50}, {1, 20}, //Bis P
                {8, 40}, {7, 50}, {7, 20}, {7, 40}, {7, 04}, {6, 50}, {7, 30}, {7, 30}, {5, 60}, {1, 20}, //Bis Q
                {4, 20}, {4, 20}, {5, 60}, {6, 20}, {6, 20}, {8, 30}, {2, 30}, {5, 20}, {2, 30}, {1, 20}, //Bis R
                {2, 50}, {5, 50}, {5, 20}, {2, 30}, {2, 30}, {2, 20}, {5, 50}, {5, 20}, {4, 30}, {1, 20}, //Bis S
                {5, 50}, {5, 50}, {2, 40}, {2, 30}, {6, 90}, {6, 40}, {8, 30}, {8, 04}, {2, 50}, {1, 20}, //Bis T

                {5, 50}, {5, 30}, {5, 40}, {2, 30}, {2, 10}, {5, 20}, {2, 70}, {5, 00}, {3, 50}, {1, 20}, //Bis U
                {8, 50}, {8, 20}, {5, 00}, {8, 30}, {0, 90}, {3, 50}, {8, 00}, {7, 30}, {7, 27}, {1, 20}, //Bis V
                {7, 30}, {7, 16}, {7, 03}, {8, 30}, {7, 50}, {7, 19}, {7, 50}, {7, 50}, {7, 07}, {1, 20}, //Bis W
                {2, 40}, {2, 30}, {5, 20}, {3, 70}, {5, 40}, {6, 50}, {5, 60}, {2, 30}, {5, 60}, {1, 20}, //Bis X
                {5, 50}, {5, 50}, {2, 40}, {2, 30}, {6, 90}, {6, 40}, {2, 50}, {2, 20}, {2, 50}, {1, 20}, //Bis Y

                {8, 13}, {5, 00}, {8, 20}, {5, 00}, {8, 40}, {8, 20}, {5, 30}, {5, 90}, {5, 90}, {1, 60}, //bis Z
        };

        public Typverwaltung() {
            aktionsListe = new ArrayList<Typ>();
            reset();
        }

        private Typ erzeugeTyp(final int index) {
            if (index < levelDesign.length && index >= 0) {
                Typ typ = null;
                final int abstand = levelDesign[index][1];
                switch (levelDesign[index][0]) {
                    case NIX:
                        typ = new NullTyp(car);
                        break;
                    case CHECKPOINT:
                        typ = new Checkpoint(car);
                        break;
                    case HAUFENNORMAL:
                        typ = new Haufen(car, Haufen.NORMAL);
                        break;
                    case HAUFENKLEIN:
                        typ = new Haufen(car, Haufen.KLEIN);
                        break;
                    case HAUFENGROSS:
                        typ = new Haufen(car, Haufen.GROSS);
                        break;
                    case LOCHNORMAL:
                        typ = new Loch(car, Loch.NORMAL);
                        break;
                    case LOCHGROSS:
                        typ = new Loch(car, Loch.GROSS);
                        break;
                    case MINE:
                        typ = new Mine(car);
                        break;
                    case UFO:
                        typ = new UfoTyp(car);
                        break;
                }
                return typ;
            } else
                return null;
        }

        /**
         * Reset dieser Klasse (nur checkpoints bleiben erhalten)
         * -> siehe checkpointreset()
         */
        public void reset() {
            if (Checkpoint.gibNummerAktiv() == 0)
                reihenfolge = 1;
            else
                reihenfolge = Checkpoint.gibNummerAktiv();
            aktionsListe.clear();
        }

        private void reihenfolge() {
            int tempReihenfolge = reihenfolge;
            while (tempReihenfolge > levelDesign.length)
                tempReihenfolge -= levelDesign.length;
            if (car.hauptAktion()) {
                bereinigeAktionsListe();
                Typ typ = null;
                Typ vorgaengerTyp = null;
                final int abstand = levelDesign[tempReihenfolge - 1][1] * 10;
                if (!aktionsListe.isEmpty())
                    vorgaengerTyp = aktionsListe.get(aktionsListe.size() - 1);
                else
                    vorgaengerTyp = null;
                if (vorgaengerTyp == null || vorgaengerTyp.xP() < Typ.STARTPOSX - abstand - vorgaengerTyp.groesse()) {
                    typ = erzeugeTyp(tempReihenfolge - 1);
                    typ.setzeNummer(reihenfolge);
                    if (typ instanceof Checkpoint)
                        ((Checkpoint) typ).aktiv = Checkpoint.gibNummerAktiv() == reihenfolge;
                    reihenfolge++;
                    aktionsListe.add(typ);
                }
            }
        }

        private void bereinigeAktionsListe() {
            for (int i = 0; i < aktionsListe.size(); i++) {
                if (aktionsListe.get(i).isCloseable())
                    aktionsListe.remove(i);
            }
        }

        public void berechne(final double faktor) {
            reihenfolge();
            Typ element = null;
            for (int i = 0; i < aktionsListe.size(); i++) {
                element = aktionsListe.get(i);
                if (element != null)
                    element.berechne(faktor);
            }
        }

        /**
         * ZEICHNEN
         */
        public void zeichneUnterAuto(final Graphics2D g) {
            Typ element = null;
            for (int i = 0; i < aktionsListe.size(); i++) {
                element = aktionsListe.get(i);
                if (element instanceof Loch || element instanceof Checkpoint)
                    element.zeichne(g);
            }
        }

        /**
         * ZEICHNEN
         */
        public void zeichneUeberAuto(final Graphics2D g) {
            Typ element = null;
            for (int i = 0; i < aktionsListe.size(); i++) {
                element = aktionsListe.get(i);
                if (!(element instanceof Loch || element instanceof Checkpoint || element == null))
                    element.zeichne(g);
            }
            if (nummer() == (bonusPointsKassiert + 1) * 260 + 1) {
                g.setColor(Color.RED);
                final Font fontStandard = g.getFont();
                final Font fontBigger = g.getFont().deriveFont(60f);
                g.setFont(fontBigger);
                g.drawString("BONUS POINTS", 250, 150);
                Werte.erhoehePunkteUm(14);
                g.setFont(fontStandard);
            }
            if (nummer() == (bonusPointsKassiert + 1) * 260 + 2)
                bonusPointsKassiert++;
        }

        public void checkpointreset() {
            Checkpoint.reset();
            bonusPointsKassiert = 0;
            reset();
        }

        public int nummer() {
            int nummer = -1; //-1 => fehler
            Object element = null;
            if (!aktionsListe.isEmpty())
                element = aktionsListe.get(aktionsListe.size() - 1);
            if (element != null)
                nummer = ((Typ) element).nummer();
            return nummer;
        }

    }

}
