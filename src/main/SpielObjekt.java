package main;

import myLib.Punkt;

import java.awt.Graphics2D;

/**
 * Abstract class Klasse - write a description of the class here
 *
 * @author Florian Hallay
 * @version 5.5.2013
 */
public abstract class SpielObjekt {

    protected Punkt pos;

    protected int groesse;

    protected boolean closeable;

    protected SpielObjekt() {
        pos = new Punkt();
    }

    protected void setCloseable() {
        closeable = true;
    }

    protected boolean isCloseable() {
        return closeable;
    }

    public abstract void zeichne(Graphics2D g);

    public abstract void berechne(double faktor);

    protected abstract void bewege(double faktor);

    public int xP() {
        return pos.getIntX();
    }

    public int yP() {
        return pos.getIntY();
    }

    public void setPosition(final double x, final double y) {
        pos = new Punkt(x, y);
    }

    public int groesse() {
        return groesse;
    }

}
