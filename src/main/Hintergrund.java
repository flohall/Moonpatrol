package main;

import myLib.Datei;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;

/**
 * Diese Klasse berechnet und zeichnet die beiden Hintergruende
 * die laenge des Bildes muss 4000 pixel sein
 * Die ersten 1000 laengen Pixel muessen den letzen 1000 entsprechen
 *
 * @author Florian Hallay
 * @version 28.01.2013
 */
public class Hintergrund {

    // Bezugsobjekte
    private final Image hintergrund;

    private final Image vordergrund;

    private double vordergrundXP;

    private double hintergrundXP;

    // Konstruktor
    public Hintergrund() {
        vordergrund = Datei.leseBild("vorne.gif");
        hintergrund = Datei.leseBild("hinten.gif");
        reset();
    }

    public void reset() {
        vordergrundXP = 0;
        hintergrundXP = 0;
    }

    public void berechne(final double faktor) {
        if (faktor != 0) {
            final double speed = Werte.hindernisSpeed() * faktor;
            final double speedHintergrundVorne = speed / 3;
            final double speedHintergrundHinten = speed / 9;
            if (vordergrundXP < -3000)
                vordergrundXP = 0;
            vordergrundXP = vordergrundXP - speedHintergrundVorne;
            if (hintergrundXP < -3000)
                hintergrundXP = 0;
            hintergrundXP = hintergrundXP - speedHintergrundHinten;
        }
    }

    public void zeichneDrunter(final Graphics2D g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, 1000, 450);

        g.drawImage(hintergrund, (int) hintergrundXP, 0, Datei.observer());
        g.drawImage(vordergrund, (int) vordergrundXP, 0, Datei.observer());
    }

    public void zeichneDrueber(final Graphics2D g) {
        g.setColor(new Color(254, 166, 74));
        g.fillRect(0, Werte.BODENHOEHE, 1000, 60);
        g.setColor(Color.BLACK);
        g.drawLine(0, Werte.BODENHOEHE, 999, 390);
        g.setColor(Color.YELLOW);
    }

}
