package main;

import myLib.BildAnimator;
import myLib.Datei;
import myLib.Punkt;
import myLib.Rechner;
import myLib.sound.Ton;

import java.awt.Graphics2D;
import java.awt.Image;

/**
 * Ein Hindernis: Der Geroellhaufen (3 verschiedene Groessen) pos aus
 * Spielobjekt bezieht sich auf vordere Ecke
 *
 * @author Florian Hallay
 * @version 05.05.2013
 */
public class Haufen extends Hindernis {

    // Konstanten
    public static final int KLEIN = 20;

    public static final int NORMAL = 40;

    public static final int GROSS = 50;

    private static Image staticKleinBild;

    private static Image staticNormalBild;

    private static Image staticGrossBild;

    private static BildAnimator staticAni;

    private static Ton staticTon;

    // Objekte
    private final Laser laser;

    private final BildAnimator bummAni;

    private Image bummBild;

    private boolean kleinbummAktiv;

    private boolean halbBildAktiv;

    private Punkt bummPunkt;

    private Image haufenBild;

    private Image halbBild;

    private final Ton bummTon;

    public Haufen(final Auto pAuto, final int pGroesse) {
        super(pAuto, pGroesse);
        laser = car.laser;
        // Bilder nur einmal Laden
        // in der If-Bedingung muss der Ton auch stehen!!
        if (staticKleinBild == null || staticNormalBild == null || staticGrossBild == null
                || staticAni == null || staticTon == null) {
            staticKleinBild = Datei.leseBild("haufen20.gif");
            staticNormalBild = Datei.leseBild("haufen40.gif");
            staticGrossBild = Datei.leseBild("haufen50.gif");
            staticAni = new BildAnimator("haufenExplosion", ".gif", 6);
            staticTon = new Ton("bumm3.wav", false);
        }
        if (groesse == KLEIN)
            haufenBild = staticKleinBild;
        else if (groesse == NORMAL)
            haufenBild = staticNormalBild;
        else if (groesse == GROSS) {
            haufenBild = staticGrossBild;
            halbBild = staticNormalBild;
        } else
            System.err.println("class Haufen NumberFromatException:"
                    + "\n..in constructor [pGroesse] = " + pGroesse + "\n..object " + this);
        bummAni = (BildAnimator) staticAni.clone();
        bummBild = bummAni.startBild();
        bummTon = staticTon;
    }

    @Override
    protected void zeichneTeil(final Graphics2D g) {
        if (halbBildAktiv) {
            final int bildHeight = halbBild.getHeight(null);
            g.drawImage(halbBild, pos.getIntX(), pos.getIntY() - bildHeight, null);
        } else {
            final int bildHeight = haufenBild.getHeight(null);
            g.drawImage(haufenBild, pos.getIntX(), pos.getIntY() - bildHeight, null);
        }
    }

    @Override
    protected void berechneTeil(final double faktor) {
    } // tue nichts

    @Override
    protected void setzeTreffer() {
        if (obenPos().y < car.yP() && obenPos().x < car.xP() && obenPos().x > car.xP()
                - car.laenge
                || Rechner.abstand(obenPos().x, obenPos().y, car.reifen.xP1,
                car.reifen.yP1) < car.reifen.radius1
                || Rechner.abstand(obenPos().x, obenPos().y, car.reifen.xP2,
                car.reifen.yP2) < car.reifen.radius2
                || Rechner.abstand(obenPos().x, obenPos().y, car.reifen.xP3,
                car.reifen.yP3) < car.reifen.radius3)
            car.setzeGetroffen(true);
    }

    @Override
    public void zeichneKleinbumm(final Graphics2D g) {
        if (kleinbummAktiv) {
            final int height = bummBild.getHeight(null);
            final int width = bummBild.getWidth(null);
            final Punkt p = new Punkt(bummPunkt.x - width / 2, bummPunkt.y - height / 2);
            g.drawImage(bummBild, p.getIntX(), p.getIntY(), null);

        }
    }

    @Override
    public void bearbeiteKleinbumm(final double speed, final double faktor) {
        if (!kleinbummAktiv) {
            if (laser.feuert() && obenPos().x < laser.xPVorne() && obenPos().y < laser.yPUnten()
                    && obenPos().x > car.xP()) {
                if (Werte.musikAn())
                    bummTon.play();
                if (groesse == GROSS && !halbBildAktiv) {
                    halbBildAktiv = true;
                    pos.x += 30;
                    laser.bodenLaserNeu();
                } else {
                    bummPunkt = new Punkt(laser.xPMitte(), laser.yPMitte());
                    setzePunkte(true);
                    laser.bodenLaserNeu();
                    kleinbummAktiv = true;
                    destroy();
                }
            }
        } else {
            bummBild = bummAni.aktuellesBild(5, faktor);
            bummPunkt.x -= speed;
            if (bummAni.kompletterZyklus()) {
                kleinbummAktiv = false;
                setCloseable();
            }
        }
    }

    /**
     * Ungef�hr die obere Spitze des Haufens
     */
    private Punkt obenPos() {
        final double xOben;
        final double yOben;
        xOben = pos.x + groesse / 2;
        if (halbBildAktiv)
            yOben = pos.y - halbBild.getHeight(null);
        else
            yOben = pos.y - haufenBild.getHeight(null);
        return new Punkt(xOben, yOben);
    }

}
