package main;

import java.awt.Graphics2D;

/**
 * Write a description of class NullType here.
 *
 * @author Florian Hallay
 * @version 01.05.2013
 */
public class NullTyp extends Typ {

    public NullTyp(final Auto pAuto) {
        super(pAuto, 0);
    }

    @Override
    public void zeichne(final Graphics2D g) {
        //es wird nichts gezeichnet
    }

    @Override
    protected void berechneTeil(final double faktor) {
    }

}
