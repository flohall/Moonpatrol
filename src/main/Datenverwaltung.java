package main;

import myLib.Datei;
import myLib.Schluessel;

/**
 * @author Florian Hallay
 * @version 25.11.2012
 */
public class Datenverwaltung {

    private static final String logfileName = "moonLogfile.txt";

    private static final String settingsFileName = "settings.txt";

    private static String urlLesen;

    private static String urlSchreiben;

    private static String urlHomepage;

    public static void schreibeLogfile(String inhalt) {
        inhalt = Schluessel.verschluesseln(inhalt);
        Datei.schreibeTextdokument(logfileName, inhalt);
    }

    public static String leseLogfile() {
        String inhalt = null;
        inhalt = Datei.leseTextdokument(logfileName);
        if (inhalt == null) {
            return null;
        } else {
            inhalt = Schluessel.entschluesseln(inhalt);
            if (inhalt != "XXfehlerXX")
                return inhalt;
            else {
                System.err.println(logfileName + " konnte nicht entschluesselt werden");
                loescheLogfile();
                return null;
            }
        }
    }

    private static String leseSettings() {
        String inhalt = null;
        inhalt = Datei.leseTextdokument(settingsFileName);
        return inhalt;
    }

    private static void interpretiereSettings() {
        String str = Datei.leseTextdokument(settingsFileName);
        if (str != null) {
            String[] tempStrings = str.split("!!");
            str = tempStrings[tempStrings.length - 1];
            tempStrings = str.split("<>");
            String tempString = null;
            for (int i = 0; i < tempStrings.length; i++) {
                tempString = tempStrings[i];
                tempString = tempString.trim();
                if (tempString.startsWith("read:"))
                    urlLesen = tempString.replaceAll("read:", "");
                else if (tempString.startsWith("write:"))
                    urlSchreiben = tempString.replaceAll("write:", "");
                else if (tempString.startsWith("homepage:"))
                    urlHomepage = tempString.replaceAll("homepage:", "");
            }
        }
    }

    public static String urlFuerLesen() {
        if (urlLesen == null)
            interpretiereSettings();
        return urlLesen;
    }

    public static String urlFuerSchreiben() {
        if (urlSchreiben == null)
            interpretiereSettings();
        return urlSchreiben;
    }

    public static String urlHomepage() {
        if (urlHomepage == null)
            interpretiereSettings();
        return urlHomepage;
    }

    public static int leseRekord() {
        if (Werte.getEnvironment() == Werte.APPLICATION) {
            String inhalt = null;
            inhalt = leseLogfile();
            if (inhalt == null)
                return 0;
            else {
                try {
                    return Integer.valueOf(inhalt);
                } catch (final NumberFormatException e3) {
                    System.err.println(logfileName + " falsche Daten enthalten");
                    loescheLogfile();
                    return 0;
                }
            }
        } else
            return 0;
    }

    public static void schreibeRekord(final int rekord) {
        if (Werte.getEnvironment() == Werte.APPLICATION)
            schreibeLogfile("" + rekord);
        //         else if(Werte.getEnvironment()==Werte.WEBSTART)
        //             
    }

    public static void loescheLogfile() {
        Datei.loescheTextdokument(logfileName);
    }

}
