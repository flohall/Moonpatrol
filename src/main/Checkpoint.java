package main;

import myLib.sound.Ton;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

/**
 * @author Florian Hallay
 * @version 05.05.2013
 */
public class Checkpoint extends Typ {

    private static int nummerAktiv;

    public boolean aktiv;

    private boolean blink;

    private int durchgang;

    final char[] chars =
            {'A', 'B', 'C', 'D', 'E', 'F'
                    , 'G', 'H', 'I', 'J', 'K', 'L'
                    , 'M', 'N', 'O', 'P', 'Q', 'R'
                    , 'S', 'T', 'U', 'V', 'W', 'X'
                    , 'Y', 'Z'};

    // Konstruktor
    public Checkpoint(final Auto pAuto) {
        super(pAuto, 30);
    }

    // Dienste
    public static int gibNummerAktiv() {
        return nummerAktiv;
    }

    public static void reset() {
        nummerAktiv = 0;
    }

    private void setzeAktiv() {
        if (!aktiv && xP() < car.xP()) {
            aktiv = true;
            nummerAktiv = nummer;
            if (Werte.musikAn()) {
                new Ton("checkpoint3.wav", true).play();
            }
        }
    }

    private char buchstabe() {
        int charIndex = nummer / 10;
        if (charIndex >= 0) {
            while (charIndex > chars.length)
                charIndex = charIndex - chars.length;
            return chars[charIndex - 1];
        } else
            return '1';
    }

    @Override
    public void zeichne(final Graphics2D g) {
        final Font fontStandard = g.getFont();
        final Font fontBig = new Font("Courier New", Font.BOLD, 30);
        final Rectangle quad = new Rectangle(pos.getIntX(), 400, groesse, groesse);
        final FontMetrics metrics = g.getFontMetrics(fontBig);
        final int xRand = (groesse - metrics.charWidth(buchstabe())) / 2; //auch als yRand

        g.setFont(fontBig);
        g.setColor(Color.GREEN);
        g.fill(quad);
        g.setColor(Color.BLACK);
        g.draw(quad);
        if (aktiv && blink)
            g.setColor(Color.RED);
        else
            g.setColor(Color.BLACK);

        g.drawString("" + buchstabe(), pos.getIntX() + xRand, 400 + groesse - xRand);
        g.setFont(fontStandard);
    }

    @Override
    protected void berechneTeil(final double faktor) {
        blinken();
    }

    @Override
    protected void bewege(final double faktor) {
        super.bewege(faktor);
        setzeAktiv();
    }

    private void blinken() {
        durchgang++;
        if (blink & durchgang >= 10 || !blink & durchgang >= 20) {
            durchgang = 0;
            blink = !blink;
        }
    }

}
