package main.tests;

import main.MainPanel;
import main.Werte;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

/**
 * Ein Eingabefenster, welches Manipulationen zu Testzwecken am Spiel ermoeglicht
 *
 * @author Florian Hallay
 * @version 01.06.2013
 */
public class Terminal extends JPanel {

    final JTextArea terminalArea;

    static final String user = "MOONPATROL\\USER>";

    static final String admin = "MOONPATROL\\ADMIN>";

    static final String passwort = "mondpass";

    boolean passwortAbfrage;

    String passwortEingabe = "";

    String standardText = user;

    String letzterText = "";

    final LetzteBefehleList<String> letzteBefehle;

    private final MainPanel rootPanel;

    public Terminal(final MainPanel panel) {
        rootPanel = panel;
        setVisible(false);
        setFocusable(false);
        setOpaque(false);
        setLayout(new java.awt.GridLayout());
        terminalArea = new JTextArea();
        terminalArea.setOpaque(false);
        terminalArea.setForeground(Color.YELLOW);
        terminalArea.setFont(new java.awt.Font("Courier New", java.awt.Font.PLAIN, 11));
        terminalArea.setBorder(javax.swing.BorderFactory.createEmptyBorder(10, 10, 10, 10));
        terminalArea.setCaretColor(Color.RED);

        terminalArea.setCursor(Cursor.getDefaultCursor());
        final MouseListener[] mls = terminalArea.getListeners(MouseListener.class);
        for (int i = 0; i < mls.length; i++)
            terminalArea.removeMouseListener(mls[i]);
        final MouseMotionListener[] mmls = terminalArea.getListeners(MouseMotionListener.class);
        for (int i = 0; i < mmls.length; i++)
            terminalArea.removeMouseMotionListener(mmls[i]);
        terminalArea.addKeyListener(new Eingabeverwaltung());
        add(terminalArea);
        letzteBefehle = new LetzteBefehleList<String>();
        letzteBefehle.toFirst();
    }

    public void aktiviere(final boolean flag) {
        if (flag) {
            setVisible(true);
            rootPanel.halteAn(main.Pause.EINGABE);
            terminalArea.requestFocus();
            terminalArea.setText(standardText);
            letzterText = "";
        } else {
            setVisible(false);
            passwortAbfrage = false;
        }
    }

    public void eingabetaste() {
        boolean nichtGefunden = false;
        boolean hilfe = false;
        boolean loeschen = false;
        boolean userEinloggen = false;
        if (passwortAbfrage)
            passwortAbfrage();
        else {
            final String eingabe = terminalArea.getText();
            final int position = eingabe.lastIndexOf(standardText);
            final String befehlString = eingabe.substring(position + standardText.length());
            terminalArea.setText(letzterText);
            //Befehle

            if (befehlString.equals("reset")) {
                Werte.setzeStartWerte();
                userEinloggen = true;
                rootPanel.game.komplettReset();
            } else if (befehlString.equals("disco"))
                Werte.setzeWirbelSpeed(30);
            else if (befehlString.equals("delete"))
                loeschen = true;
            else if (befehlString.equals("help"))
                hilfe = true;
            else if (befehlString.equals("debug")) {
                if (!Werte.adminRechte())
                    Werte.setDebugMode(true);
            } else if (befehlString.equals("admin"))
                passwortAbfrage = true;
            else if (befehlString.equals("user"))
                userEinloggen = true;
            else if (Werte.adminRechte()) {
                if (befehlString.equals("100"))
                    Werte.erhoehePunkteUm(100);
                else if (befehlString.equals("god"))
                    Werte.setzeUnbesiegbar(true);
                else if (befehlString.equals("sanni"))
                    Werte.setzeVollesLeben();
                else if (befehlString.equals("life"))
                    Werte.setzeLeben(100);
                else
                    nichtGefunden = true;
            } else
                nichtGefunden = true;

            if (nichtGefunden)
                terminalArea.append(standardText + befehlString + "  [not found]");

            else {
                terminalArea.append(standardText + befehlString + "... ");
                for (int i = 0; i < letzteBefehle.size(); i++) {
                    if (letzteBefehle.get(i).equals(befehlString)) {
                        letzteBefehle.remove(i);
                        break;
                    }
                }
                letzteBefehle.add(0, befehlString);
            }
            terminalArea.append("\n");
            if (hilfe)
                terminalArea.append(
                        "  help:\t\tlist of commands\n" +
                                "  delete:\tdelete console\n" +
                                "  reset:\tgame reset\n" +
                                "  debug:\tshow bugs\n" +
                                "  up and down:\tlast commands\n");
            else if (loeschen)
                terminalArea.setText("");
        }
        //Begrenzung auf 10 Reihen
        try {
            if (terminalArea.getLineOfOffset(terminalArea.getCaretPosition()) > 10) {
                terminalArea.setText(terminalArea.getText().substring(terminalArea.getText().indexOf("\n") + 1));
            }
        } catch (final javax.swing.text.BadLocationException ex) {
        }
        if (userEinloggen) {
            standardText = user;
            Werte.setzeAdminRechte(false);
            Werte.setzeUnbesiegbar(false);
        }
        letzterText = terminalArea.getText();
        if (!passwortAbfrage)
            terminalArea.append(standardText);
        letzteBefehle.toFirst();
    }

    private void passwortAbfrage() {
        if (passwortEingabe.equals(passwort)) {
            standardText = admin;
            Werte.setzeAdminRechte(true);
            Werte.setDebugMode(false);
        }
        passwortEingabe = "";
        terminalArea.setText(letzterText);
        passwortAbfrage = false;
    }

    public void letztenBefehlAnzeigen() {
        if (!passwortAbfrage) {
            terminalArea.setText(letzterText);
            terminalArea.append(standardText);
            letzteBefehle.back();
            terminalArea.append(letzteBefehle.getCurrent());
        }
    }

    public void vorherigenBefehlAnzeigen() {
        if (!passwortAbfrage) {
            terminalArea.setText(letzterText);
            terminalArea.append(standardText);
            letzteBefehle.next();
            terminalArea.append(letzteBefehle.getCurrent());
        }
    }

    class Eingabeverwaltung extends KeyAdapter {

        boolean strgGedrueckt;

        public Eingabeverwaltung() {
        }

        @Override
        public void keyPressed(final KeyEvent e) {
            final String eingabe = terminalArea.getText();
            final int position = eingabe.lastIndexOf(standardText);
            switch (e.getKeyCode()) {
                case KeyEvent.VK_DEAD_CIRCUMFLEX:
                    aktiviere(false);
                    rootPanel.fuehreAus();
                    e.consume();
                    break;
                case KeyEvent.VK_ENTER:
                    eingabetaste();
                    e.consume();
                    break;
                case KeyEvent.VK_BACK_SPACE:
                    if (strgGedrueckt)
                        e.consume();
                    if (eingabe.endsWith(standardText) || eingabe.endsWith("\n")
                            || terminalArea.getCaretPosition() <= position + standardText.length())
                        e.consume();
                    if (passwortAbfrage && passwortEingabe.length() > 0) {
                        final StringBuffer sBuff = new StringBuffer(passwortEingabe);
                        sBuff.setLength(passwortEingabe.length() - 1);
                        passwortEingabe = sBuff.toString();
                    }
                    break;
                case KeyEvent.VK_LEFT:
                    if (strgGedrueckt)
                        e.consume();
                    if (eingabe.endsWith(standardText) || terminalArea.getCaretPosition() <= position + standardText.length())
                        e.consume();
                    if (passwortAbfrage)
                        e.consume();
                    break;
                case KeyEvent.VK_UP:
                    vorherigenBefehlAnzeigen();
                    e.consume();
                    break;
                case KeyEvent.VK_DOWN:
                    letztenBefehlAnzeigen();
                    e.consume();
                    break;
                case KeyEvent.VK_CONTROL:
                    strgGedrueckt = true;
                    break;
                case KeyEvent.VK_A:
                    if (strgGedrueckt)
                        e.consume();
                    break;
                default:
                    letzteBefehle.toFirst();
                    break;
            }
            rootPanel.repaint();
        }

        @Override
        public void keyTyped(final KeyEvent e) {
            switch (e.getKeyChar()) {
                case '\\':
                    e.consume();
                    break;
                default:
                    if (passwortAbfrage && Character.isLetterOrDigit(e.getKeyChar())) {
                        passwortEingabe += e.getKeyChar();
                        e.consume();
                        terminalArea.append("*");
                    }
                    break;
            }
            rootPanel.repaint();
        }

        @Override
        public void keyReleased(final KeyEvent e) {
            switch (e.getKeyCode()) {
                case KeyEvent.VK_CONTROL:
                    strgGedrueckt = false;
                    break;
            }
        }

    }

    static class LetzteBefehleList<T> extends ArrayList<T> {

        int index;

        public LetzteBefehleList() {
            index = 0;
        }

        public void next() {
            if (index < size() - 1)
                index++;
        }

        public void back() {
            if (index < 0)
                index = 0;
            if (index > 0)
                index--;
        }

        public T getCurrent() {
            return get(index);
        }

        public void toFirst() {
            index = -1;
        }

    }

}
