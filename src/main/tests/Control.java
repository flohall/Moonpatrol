package main.tests;

import main.MainPanel;
import main.Werte;
import myLib.Logbuch;
import myLib.Rechner;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Ein Ausgabefenster, welches Spielinterne Informationen zu Testzwecken darstellt
 *
 * @author Florian Hallay
 * @version 28.12.2012
 */
public class Control extends JPanel {

    private int mausXP;

    private int mausYP;

    private int mausXPVorgaenger;

    private int mausYPVorgaenger;

    private final MainPanel rootPanel;

    /**
     * Constructor for objects of class Control
     */
    public Control(final MainPanel panel) {
        rootPanel = panel;
        setVisible(true);
        setFocusable(false);
        setOpaque(false);
        addMouseListener(new Mausverwaltung());
    }

    @Override
    public void paintComponent(final Graphics g) {
        g.setFont(new java.awt.Font("Courier New", java.awt.Font.PLAIN, 11));
        if (Werte.adminRechte()) {
            g.setColor(Color.RED);
            g.drawString("Admin-Mode  TestPanel", 400, 15);
            g.setColor(Color.YELLOW);
            g.drawString("USER:", 400, 60);
            g.drawString("help: able", 400, 70);
            g.drawString("delete: able", 400, 80);
            g.drawString("reset: able", 400, 90);
            g.drawString("user: " + !Werte.adminRechte(), 400, 100);
            g.drawString("admin: " + Werte.adminRechte(), 400, 110);
            g.drawString("disco: " + (Werte.wirbelSpeed() > 0), 400, 120);
            g.drawString("debug: able", 400, 140);
            g.drawString("ADMIN:", 500, 60);
            g.drawString("god: " + Werte.unbesiegbar(), 500, 70);
            g.drawString("sanni: able", 500, 80);
            g.drawString("100: able", 500, 90);
            g.drawString("life: " + (Werte.leben() > 3), 500, 100);
            g.drawString("fps[+,-]: " + Werte.gibFps(), 500, 110);
            g.setColor(Color.RED);
            final java.util.ArrayList<String> strings = Logbuch.getAusgabe();
            if (strings != null) {
                for (int i = 0; i < strings.size(); i++) {
                    if (strings.get(i).startsWith("FINER"))
                        g.setColor(Color.GRAY);
                    else if (strings.get(i).startsWith("FINE"))
                        g.setColor(Color.WHITE);
                    else if (strings.get(i).startsWith("INFO"))
                        g.setColor(Color.GREEN);
                    else if (strings.get(i).startsWith("WARNING"))
                        g.setColor(Color.YELLOW);
                    else if (strings.get(i).startsWith("SEVERE"))
                        g.setColor(Color.RED);
                    g.drawString(strings.get(i), 600, 60 + i * 10);
                    i++;
                    g.drawString(strings.get(i), 600, 60 + i * 10);
                }
            }
        }
        if (Werte.adminRechte() || Werte.getDebugMode()) {
            g.setColor(Color.YELLOW);
            g.drawString("Maus X " + mausXP, 400, 30);
            g.drawString("Maus Y " + mausYP, 400, 40);

            g.drawString("Abstand " + Math.round(Rechner.abstand(mausXP, mausYP, mausXPVorgaenger, mausYPVorgaenger) * 100) / 100.0, 490, 30);
            g.drawString("Nummer des Rechten Objekts: " + rootPanel.game.nummer(), 600, 30);
            g.drawString("HindernisSpeed: " + Werte.hindernisSpeed(), 600, 40);

        }
        if (Werte.getDebugMode()) {
            g.setColor(Color.YELLOW);
            g.drawString("fps: " + Werte.gibFps(), 850, 30);
            g.setColor(Color.RED);
            g.drawString("Debug-Mode  TestPanel", 400, 15);
            final java.util.ArrayList<String> strings = Logbuch.getAusgabe();
            if (strings != null) {
                for (int i = 0; i < strings.size(); i++) {
                    if (strings.get(i).startsWith("FINER"))
                        g.setColor(Color.GRAY);
                    else if (strings.get(i).startsWith("FINE"))
                        g.setColor(Color.WHITE);
                    else if (strings.get(i).startsWith("INFO"))
                        g.setColor(Color.GREEN);
                    else if (strings.get(i).startsWith("WARNING"))
                        g.setColor(Color.YELLOW);
                    else if (strings.get(i).startsWith("SEVERE"))
                        g.setColor(Color.RED);
                    g.drawString(strings.get(i), 400, 60 + i * 10);
                    i++;
                    g.drawString(strings.get(i), 400, 60 + i * 10);
                }
            }
        }
    }

    /**
     * Klasse die bestimmt was Mauseingaben bewirken.
     * Muss einem MouseListener uebergeben werden.
     * (implements MouseListener auch moeglich
     * benoetigt aber leere Methoden)
     */
    class Mausverwaltung extends MouseAdapter {

        public Mausverwaltung() {
        }

        /**
         * Ueberschreibt nur mousePressed
         */
        @Override
        public void mousePressed(final MouseEvent e) {
            mausXPVorgaenger = mausXP;
            mausYPVorgaenger = mausYP;
            mausXP = e.getX();
            mausYP = e.getY();
            rootPanel.repaint();
        }

    }

}
