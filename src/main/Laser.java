package main;

import myLib.BildAnimator;
import myLib.Punkt;
import myLib.sound.Ton;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;

/**
 * @author Florian Hallay
 * @version 15.04.2013
 */
public class Laser {

    private final AirLaser[] airLaser;

    private final BodenLaser bodenLaser;

    public Laser() {
        airLaser = new AirLaser[6];
        for (int i = 0; i < airLaser.length; i++)
            airLaser[i] = new AirLaser();
        bodenLaser = new BodenLaser();
        neu();
    }

    public void neu() {
        for (int i = 0; i < airLaser.length; i++)
            airLaser[i].neu();
        bodenLaser.neu();
    }

    public void bodenLaserNeu() {
        bodenLaser.neu();
    }

    public void zeichne(final Graphics2D g) {
        for (int i = 0; i < airLaser.length; i++)
            airLaser[i].zeichne(g);
        bodenLaser.zeichne(g);
    }

    public void bewege(final int x, final int y, final double faktor) {
        for (int i = 0; i < airLaser.length; i++)
            airLaser[i].bewege(x, y, faktor);
        bodenLaser.bewege(x, y, faktor);
    }

    public void setzeSollFeuern(final boolean p) {
        int index;
        int warteZeit = 0;
        for (int i = 0; i < airLaser.length; i++) {
            if (airLaser[i].feuert() && airLaser[i].nachfolgerSollWarten() > warteZeit) {
                warteZeit = airLaser[i].nachfolgerSollWarten();
                break;
            }
        }
        for (int i = 0; i < airLaser.length; i++) {
            if (!airLaser[i].feuert()) {
                airLaser[i].setzeSollFeuern(p, warteZeit);
                break;
            }
        }
        bodenLaser.setzeSollFeuern(p);

    }

    public int[] xP() {
        final int[] x = new int[airLaser.length];
        for (int i = 0; i < airLaser.length; i++)
            x[i] = airLaser[i].xP();
        return x;
    }

    public int[] yP() {
        final int[] y = new int[airLaser.length];
        for (int i = 0; i < airLaser.length; i++)
            y[i] = airLaser[i].yP();
        return y;
    }

    /**
     * CrashpunktX BodenLaser
     */
    public int xPVorne() {
        return bodenLaser.xPVorne();
    }

    /**
     * CrashpunktY BodenLaser
     */
    public int yPUnten() {
        return bodenLaser.yPUnten();
    }

    /**
     * MittelpunktY BodenLaser
     */
    public int xPMitte() {
        return bodenLaser.xPMitte();
    }

    /**
     * MittelpunktY BodenLaser
     */
    public int yPMitte() {
        return bodenLaser.yPMitte();
    }

    /**
     * true wenn einer der Laser feuert
     */
    public boolean feuert() {
        if (!bodenLaser.feuert()) {
            boolean feuert = false;
            for (int i = 0; i < airLaser.length; i++) {
                if (airLaser[i].feuert()) {
                    feuert = true;
                    break;
                }
            }
            return feuert;
        } else
            return true;
    }

    static class AirLaser {

        private static final int feuerSpeed = 8;

        private boolean sollFeuern;

        private boolean bereit;

        private boolean feuert;

        private int verstricheneSchritte;

        private int warteZeit;

        private Punkt pos;

        // Konstruktor
        public AirLaser() {
            neu();
        }

        public void neu() {
            sollFeuern = false;
            bereit = false;
            feuert = false;
            verstricheneSchritte = 0;
            warteZeit = 0;
            pos = new Punkt();
        }

        public int nachfolgerSollWarten() {
            if (verstricheneSchritte < 5)
                return 5 - verstricheneSchritte;
            else
                return 0;
        }

        public void zeichne(final Graphics2D g) {
            if (feuert) {
                g.setColor(Color.WHITE);
                g.fillRect(pos.getIntX(), pos.getIntY(), 2, 6);
            }
        }

        public void bewege(final int xP, final int yP, final double faktor) {
            setzeFeuert();
            if (feuert) {
                if (pos.y >= 0) {
                    pos.y -= feuerSpeed * faktor;
                    verstricheneSchritte++;
                } else
                    neu();
            } else {
                pos.x = xP - 80;
                pos.y = yP - 35;
                bereit = true;
            }
        }

        public void setzeSollFeuern(final boolean p, final int warteZeit) {
            sollFeuern = p;
            this.warteZeit = warteZeit;
        }

        public boolean feuert() {
            return feuert;
        }

        private void setzeFeuert() {
            if (warteZeit > 0)
                warteZeit--;
            else if (sollFeuern && bereit) {
                feuert = true;
                sollFeuern = false;
                bereit = false;
                if (Werte.musikAn()) {
                    new Ton("shoot3.wav", true).play();
                }
            }
        }

        public int xP() {
            return pos.getIntX();
        }

        public int yP() {
            return pos.getIntY();
        }

    }

    static class BodenLaser {

        private static final int feuerWeite = 150;

        private static final int feuerSpeed = 5;

        private static final int radius = 5;

        private final BildAnimator ani;

        private Image bild;

        private boolean feuert;

        private boolean sollFeuern;

        private boolean bereit;

        private int wiederholung;

        private Punkt mittelpunkt;

        // Konstruktor
        public BodenLaser() {
            ani = new BildAnimator("bodenLaser", ".gif", 5);
            neu();
        }

        public void neu() {
            bild = ani.startBild();
            wiederholung = 0;
            sollFeuern = false;
            bereit = false;
            feuert = false;
            mittelpunkt = new Punkt();
        }

        public void zeichne(final Graphics2D g) {
            if (feuert) {
                g.drawImage(bild, mittelpunkt.getIntX() - 7, mittelpunkt.getIntY() - 5, null);
            }
        }

        public void bewege(final int xP, final int yP, final double faktor) {
            setzeFeuert();
            if (feuert) {
                if (wiederholung < feuerWeite / feuerSpeed) {
                    bild = ani.aktuellesBild(5, faktor);
                    mittelpunkt.x += feuerSpeed * faktor;
                    wiederholung++;
                } else
                    neu();
            } else {
                mittelpunkt.x = xP;
                mittelpunkt.y = yP - 12;
                bereit = true;
            }
        }

        private void setzeFeuert() {

            if (sollFeuern && bereit) {
                feuert = true;
                sollFeuern = false;
                bereit = false;
            }
        }

        public void setzeSollFeuern(final boolean p) {
            sollFeuern = p;
        }

        public boolean feuert() {
            return feuert;
        }

        public int xPVorne() {
            return mittelpunkt.getIntX() + radius;
        }

        public int yPUnten() {
            return mittelpunkt.getIntY() + radius;
        }

        public int xPMitte() {
            return mittelpunkt.getIntX();
        }

        public int yPMitte() {
            return mittelpunkt.getIntY();
        }

    }

}
