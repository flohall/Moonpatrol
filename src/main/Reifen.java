package main;

import myLib.Grafik;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

/**
 * @author Florian Hallay
 * @version 19.10.2012
 */
public class Reifen {

    // Objekte
    public final int groesse;

    public final int radius1;

    public final int radius2;

    public final int radius3;

    public int xP1;

    public int xP2;

    public int xP3;

    public int yP1;

    public int yP2;

    public int yP3;

    private final Strich strich1;

    private final Strich strich2;

    private final Strich strich3;

    private boolean bereit;

    // Konstruktor
    public Reifen() {
        this(1);
    }

    public Reifen(final int pGroesse) {
        groesse = pGroesse;
        radius1 = 10 * groesse;
        radius2 = 13 * groesse;
        radius3 = 13 * groesse;
        strich1 = new Strich();
        strich2 = new Strich();
        strich3 = new Strich();
    }

    public void neu() {
        bereit = false;
    }

    public void zeichne(final Graphics g) {
        if (bereit) {
            g.setColor(Color.black);
            Grafik.fuelleKreis(xP1, yP1, radius1, g);
            Grafik.fuelleKreis(xP2, yP2, radius2, g);
            Grafik.fuelleKreis(xP3, yP3, radius3, g);
            strich1.zeichne(g);
            strich2.zeichne(g);
            strich3.zeichne(g);
        }
    }

    public void bearbeite(final int xP, final int yP, final double autoSpeed, final boolean bodenBewegung, final double faktor) {
        xP1 = xP - 17 * groesse;
        xP2 = xP - 60 * groesse;
        xP3 = xP - 90 * groesse;
        yP1 = yP - (radius1 - 5);
        yP2 = yP - (radius2 - 5);
        yP3 = yP - (radius3 - 5);
        strich1.bearbeite(new Point(xP1, yP1), radius1, autoSpeed, bodenBewegung);
        strich2.bearbeite(new Point(xP2, yP2), radius2, autoSpeed, bodenBewegung);
        strich3.bearbeite(new Point(xP3, yP3), radius3, autoSpeed, bodenBewegung);
        bereit = true;
    }

    private static class Strich {

        private final double[] strichX = new double[4];

        private final double[] strichY = new double[4];

        private double winkel;

        public Strich() {
            winkel = 0;
        }

        public void zeichne(final Graphics g) {
            g.setColor(Color.white);
            for (int i = 0; i < strichX.length; i++) {
                final int x = Double.valueOf(strichX[i]).intValue();
                final int y = Double.valueOf(strichY[i]).intValue();
                g.drawLine(x, y, x, y);
            }
        }

        public void bearbeite(final Point mittelpunkt, final int radius, final double autoSpeed, final boolean bodenBewegung) {
            if (bodenBewegung)
                setzeWinkel(winkel - (Werte.hindernisSpeed() + Werte.wirbelSpeed() + autoSpeed) / (2 * radius * Math.PI));
            else
                setzeWinkel(winkel - autoSpeed / (2 * radius * Math.PI));
            final int r = radius - 1;
            strichX[0] = r * Math.sin(winkel) + mittelpunkt.getX();
            strichY[0] = r * Math.cos(winkel) + mittelpunkt.getY();
            strichX[1] = r * Math.sin(winkel + 2 * Math.PI / 4 * 1) + mittelpunkt.getX();
            strichY[1] = r * Math.cos(winkel + 2 * Math.PI / 4 * 1) + mittelpunkt.getY();
            strichX[2] = r * Math.sin(winkel + 2 * Math.PI / 4 * 2) + mittelpunkt.getX();
            strichY[2] = r * Math.cos(winkel + 2 * Math.PI / 4 * 2) + mittelpunkt.getY();
            strichX[3] = r * Math.sin(winkel + 2 * Math.PI / 4 * 3) + mittelpunkt.getX();
            strichY[3] = r * Math.cos(winkel + 2 * Math.PI / 4 * 3) + mittelpunkt.getY();
        }

        private void setzeWinkel(final double zahl) {
            winkel = zahl;
            if (winkel < 0)
                while (winkel < -2 * Math.PI)
                    winkel = winkel + 2 * Math.PI;
            else
                while (winkel > 2 * Math.PI)
                    winkel = winkel - 2 * Math.PI;
        }

    }

}
