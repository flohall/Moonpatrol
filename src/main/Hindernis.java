package main;

import main.interfaces.Explosive;

import java.awt.Graphics2D;

/**
 * Abstrakte Oberklasse von allen Hindernissen, welche sich auf dem Boden befinden
 *
 * @author Florian Hallay
 * @version 05.05.2013
 */
public abstract class Hindernis extends Typ implements Explosive {

    protected boolean zerstoert;

    protected boolean punkteGezaehlt;

    /**
     * ACHTUNG Unterklassen brauchen super(); im Konstruktor
     */
    public Hindernis(final Auto pAuto, final int pGroesse) {
        super(pAuto, pGroesse);
        zerstoert = false;
        punkteGezaehlt = false;
    }

    protected abstract void zeichneTeil(Graphics2D g);

    protected abstract void setzeTreffer();

    @Override
    public void zeichneKleinbumm(final Graphics2D g) {
    }

    @Override
    public void bearbeiteKleinbumm(final double speed, final double faktor) {
    }

    protected void setzePunkte(final boolean pSofort) {
        if (!punkteGezaehlt && xP() + groesse < car.xP() - car.laenge || pSofort) {
            punkteGezaehlt = true;
            Werte.erhoehePunkteUm(10);
        }
    }

    @Override
    public void destroy() {
        zerstoert = true;
        bewegeBis(1000, 390);
    }

    @Override
    public void zeichne(final Graphics2D g) {
        zeichneTeil(g);
        zeichneKleinbumm(g);
    }

    @Override
    public void berechne(final double faktor) {
        if (faktor != 0)
            bewege(faktor);
        else
            bearbeiteKleinbumm(0, faktor);
        berechneTeil(faktor);
    }

    @Override
    protected void bewege(final double faktor) {
        final double speed = Werte.hindernisSpeed() * faktor;
        if (!zerstoert) {
            bewegeUm(-speed);
            links();
            if (!Werte.unbesiegbar())
                setzeTreffer();
            setzePunkte(false);
        }
        bearbeiteKleinbumm(speed, faktor);
    }

}
