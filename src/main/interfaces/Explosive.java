package main.interfaces;

import java.awt.Graphics2D;

/**
 * Write a description of interface Explosive here.
 *
 * @author Florian Hallay
 * @version 19.10.2012
 */
public interface Explosive {

    void zeichneKleinbumm(Graphics2D g);

    void bearbeiteKleinbumm(double speed, double faktor);

}
