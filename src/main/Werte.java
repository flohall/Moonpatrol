package main;

import myLib.Logbuch;

import javax.swing.JComponent;
import java.awt.Rectangle;
import java.util.Locale;

/**
 * Statische Klasse Werte: speichert Einstellungen global ab stellt
 * Spielkonstanten bereit
 *
 * @author Florian Hallay
 * @version 11.05.2013
 */
public class Werte {

    // globale Konstanten
    public static final String VERSION = "4.5.0";

    public static final int APPLICATION = 0;

    public static final int BODENHOEHE = 390;

    // Standardgroesse von SpielPanel
    public static final int WIDTH = 1000;

    public static final int HEIGHT = 450;

    // interne Konstanten
    private static final int maxLeben = 3;

    private static final int minSpeed = 5;

    private static final int standardSpeed = 6;

    private static final int maxSpeed = 7;

    // interne Variablen
    private static boolean pauseAktiv;

    private static int environment;

    private static double hindernisSpeed;

    private static double setFps;

    private static int punkte;

    private static int rekord;

    private static int leben;

    private static boolean debugMode;

    private static boolean adminRechte;

    private static boolean unbesiegbar;

    private static boolean musikAn;

    private static String spielerName;

    // Variable um sonderbare effekte einzustellen [cheats]
    private static int wirbelSpeed;

    public static void setzeStartWerte() {
        Thread.setDefaultUncaughtExceptionHandler(Logbuch.getUncaughtEHandler());
        punkte = 0;
        pauseAktiv = false;
        rekord = Datenverwaltung.leseRekord();
        setzeVollesLeben();
        adminRechte = false;
        debugMode = false;
        unbesiegbar = false;
        musikAn = false;
        spielerName = "";
        wirbelSpeed = 0;
        setFps = 52;
        hindernisSpeed = standardSpeed;
        JComponent.setDefaultLocale(Locale.ENGLISH);
    }

    public static void setzePauseAktiv(final boolean flag) {
        pauseAktiv = flag;
    }

    public static boolean getPauseAktiv() {
        return pauseAktiv;
    }

    public static int getEnvironment() {
        return environment;
    }

    public static void setEnvironment(final int pEnvironment) {
        if (pEnvironment >= 0 && pEnvironment <= 2)
            environment = pEnvironment;
    }

    public static void setzeVollesLeben() {
        leben = maxLeben;
    }

    public static void setzeLeben(final int leb) {
        leben = leb;
    }

    public static void setzeMinusLeben() {
        leben--;
    }

    public static void setzeNullPunkte() {
        punkte = 0;
    }

    public static void erhoehePunkteUm(final int zuAddierendePunkte) {
        punkte = punkte + zuAddierendePunkte;
        aktualisiereRekord();
    }

    public static void reseteRekord() {
        rekord = punkte;
        Datenverwaltung.schreibeRekord(rekord);
    }

    public static boolean istTot() {
        return leben <= 0;
    }

    public static int punkte() {
        return punkte;
    }

    public static int rekord() {
        return rekord;
    }

    public static int leben() {
        return leben;
    }

    public static double hindernisSpeed() {
        return hindernisSpeed;
    }

    public static void setzeHindernisSpeed(final double speed) {
        hindernisSpeed = speed;
    }

    public static int maxHindernisSpeed() {
        return maxSpeed;
    }

    public static int minHindernisSpeed() {
        return minSpeed;
    }

    public static int standardHindernisSpeed() {
        return standardSpeed;
    }

    public static double gibFps() {
        return setFps;
    }

    public static void setzeFps(final double fps) {
        setFps = fps;
    }

    public static int sleepTime() {
        return (int) (1000.0 / setFps);
    }

    private static void aktualisiereRekord() {
        if (punkte > rekord) {
            rekord = punkte;
            if (Datenverwaltung.leseRekord() < rekord)
                Datenverwaltung.schreibeRekord(rekord);
        }
    }

    public static boolean adminRechte() {
        return adminRechte;
    }

    public static void setzeAdminRechte(final boolean rechte) {
        adminRechte = rechte;
    }

    public static boolean getDebugMode() {
        return debugMode;
    }

    public static void setDebugMode(final boolean flag) {
        debugMode = flag;
    }

    public static void setzeUnbesiegbar(final boolean flag) {
        unbesiegbar = flag;
    }

    public static boolean unbesiegbar() {
        return unbesiegbar;
    }

    public static void setzeMusikAn(final boolean flag) {
        musikAn = flag;
    }

    public static boolean musikAn() {
        return musikAn;
    }

    public static String spielerName() {
        return spielerName;
    }

    public static void setzeSpielerName(final String name) {
        spielerName = name;
    }

    public static Rectangle getBounds() {
        return new Rectangle(0, 0, WIDTH, HEIGHT);
    }

    public static int wirbelSpeed() {
        return wirbelSpeed;
    }

    public static void setzeWirbelSpeed(final int speed) {
        wirbelSpeed = speed;
    }

}
