package main;

import java.awt.Graphics2D;

/**
 * Abstract class Typ - write a description of the class here
 *
 * @author Florian Hallay
 * @version 01.05.2013
 */
public abstract class Typ extends SpielObjekt {

    protected final Auto car;

    protected int nummer;

    public static final int STARTPOSX = 1000;

    /**
     * ACHTUNG Unterklassen brauchen super(); im Konstruktor
     */
    public Typ(final Auto pAuto, final int pGroesse) {
        car = pAuto;
        groesse = pGroesse;
        bewegeBis(STARTPOSX, 390);
    }

    public void destroy() {
        bewegeBis(STARTPOSX, 390);
    }

    protected void bewegeBis(final int x, final int y) {
        pos.x = x;
        pos.y = y;
    }

    protected void bewegeUm(final double pSpeed) {
        pos.x += pSpeed;
    }

    @Override
    public abstract void zeichne(Graphics2D g);

    @Override
    public void berechne(final double faktor) {
        if (faktor != 0)
            bewege(faktor);
        berechneTeil(faktor);
    }

    protected abstract void berechneTeil(double faktor);

    @Override
    protected void bewege(final double faktor) {
        final double speed = Werte.hindernisSpeed() * faktor;
        bewegeUm(-speed);
        links();
    }

    public void setzeNummer(final int p) {
        nummer = p;
    }

    public int nummer() {
        return nummer;
    }

    protected void links() {
        if (pos.x + groesse < 0) {
            setCloseable();
        }
    }

}
