package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

/**
 * Klasse zum Pausieren des Spiels
 *
 * @author Florian Hallay
 * @version 11.05.2013
 */
public class Pause {

    public static final int NORMAL = 0;

    public static final int EINGABE = 1;

    public static final int GAMEOVER = 2;

    private int pausenArt;

    public Pause() {
        pausenArt = 0;
    }

    public void zeichne(final Graphics2D g) {
        if (Werte.getPauseAktiv()) {
            final Font fontStandard = g.getFont();
            final Font fontBig = g.getFont().deriveFont(20f);
            final Font fontBigger = g.getFont().deriveFont(60f);
            g.setColor(new Color(0, 0, 0, 100));
            g.fill(Werte.getBounds());
            g.setColor(Color.white);
            g.setFont(fontBig);
            switch (pausenArt) {
                case NORMAL:
                    g.drawString("PRESS 'P' TO CONTINUE", 370, 250);
                    break;
                case EINGABE:
                    g.drawString("PRESS ^ TO CONTINUE", 370, 250);
                    break;
                case GAMEOVER:
                    g.drawString("PRESS 'C' TO CONTINUE AT LAST CHECKPOINT", 250, 230);
                    g.drawString("PRESS 'N' FOR A NEW GAME", 250, 260);
                    g.setFont(fontBigger);
                    g.drawString("GAME OVER", 250, 150);
                    g.setFont(fontStandard);
                    g.drawString("(you will lose your points anyway)", 250, 310);
                    break;
            }
            g.setFont(fontStandard);
        }
    }

    public void pausiere(final int art) {
        Werte.setzePauseAktiv(true);
        if (pausenArt != GAMEOVER)
            pausenArt = art;
    }

    public void weiter() {
        Werte.setzePauseAktiv(false);
        pausenArt = 0;
    }

    public boolean pausiert(final int art) {
        return Werte.getPauseAktiv() && pausenArt == art;
    }

}

