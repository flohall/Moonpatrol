package main;

import main.interfaces.Explosive;
import myLib.BildAnimator;
import myLib.Datei;
import myLib.Punkt;
import myLib.sound.Ton;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;

/**
 * Write a description of class Ufo here.
 *
 * @author Florian Hallay
 * @version 05.05.2013
 */
public class Ufo extends SpielObjekt implements Explosive {

    private static BildAnimator staticUfoAni;

    private static BildAnimator staticBummAni;

    private Image bild;

    private final BildAnimator ufoAni;

    private boolean kleinbummAktiv;

    private final Laser laser;

    private Punkt bummPunkt;

    private final Bombe bomb;

    private final BildAnimator bummAni;

    private Image bummBild;

    private double bummSpeed;

    private boolean zerstoert;

    private double speedDurchgang;

    public Ufo(final Auto pAuto) {
        //Bilder nur einmal Laden
        if (staticUfoAni == null || staticBummAni == null) {
            staticUfoAni = new BildAnimator("ufo", ".gif", 5);
            staticBummAni = new BildAnimator("ufoExplosion", ".gif", 7);
            final Image tempBild = staticBummAni.startBild();

            Datei.warteAufBild(tempBild);

            final int height = tempBild.getHeight(null);
            final int width = tempBild.getWidth(null);

            final java.awt.image.BufferedImage buff = new java.awt.image.BufferedImage(width, height, java.awt.image.BufferedImage.TYPE_INT_ARGB);
            final Graphics2D buffG = buff.createGraphics();
            buffG.setColor(Color.RED);
            buffG.setFont(new java.awt.Font("SansSerif", java.awt.Font.PLAIN, 20));
            buffG.drawString(20 + "", width / 2 - 10, height / 2 + 7);
            buffG.dispose();

            staticBummAni.haengeBildAn(buff);
            staticBummAni.haengeBildAn(buff);
        }
        ufoAni = (BildAnimator) staticUfoAni.clone();
        bummAni = (BildAnimator) staticBummAni.clone();
        laser = pAuto.laser;
        bomb = new Bombe(pAuto);

        destroy();
        zerstoert = false;
        bummPunkt = null;
        bummSpeed = 0;
        kleinbummAktiv = false;
        bild = ufoAni.startBild();
        bummBild = bummAni.startBild();
        bomb.neu();
        bummSpeed = 0.5;
        speedDurchgang = 0;
    }

    public void destroy() {
        pos.x = -100;
        pos.y = Math.random() * 60 + 20;
        zerstoert = true;
    }

    @Override
    public void zeichne(final Graphics2D g) {
        bomb.zeichne(g);
        zeichneKleinbumm(g);
        if (!zerstoert) {
            g.drawImage(bild, pos.getIntX(), pos.getIntY(), null);
        }
    }

    @Override
    public void berechne(final double faktor) {
        bild = ufoAni.aktuellesBild(5, faktor);
        if (faktor != 0)
            bewege(faktor);
        else
            bearbeiteKleinbumm(0, faktor);
        if (Math.random() < 0.05 && !zerstoert && xP() > 130 && xP() < 600)
            bomb.setzeSollFeuern(true);
        if (zerstoert && !kleinbummAktiv && !bomb.feuert())
            setCloseable();
    }

    @Override
    protected void bewege(final double faktor) {
        //�nderungsratefunktionen mit f'(x)=ufoSpeed 
        final double x = speedDurchgang;
        double ufoSpeed = 0;
        if (x < 250) {
            ufoSpeed = -16 / 225.0 * x + 32 / 3.0;
        } else if (x < 350) {
            ufoSpeed = 32 / 225.0 * x - 128 / 3.0;
        } else if (x < 500) {
            ufoSpeed = -0.027654320988 * x + 16.79012345679;
        }
        if (!zerstoert) {
            pos.x += ufoSpeed;
            pos.y += Math.sin(Math.toRadians(x) * 2);
            if (pos.y < 0)
                pos.y = 0;
            if (pos.y > 200)
                pos.y = 200;
        }
        final int height = bild.getHeight(null);
        final int width = bild.getWidth(null);
        bomb.bewege(pos.x + width / 2, pos.y + height, ufoSpeed, faktor);
        if (speedDurchgang == 500) {
            if (pos.x < 1100 - 5 || pos.x > 1100 + 5)
                System.err.println("Fehler Ufofunktion: " + " / " + pos.x);
            destroy();
        }
        speedDurchgang += faktor;
        final double speed = Werte.hindernisSpeed();
        bearbeiteKleinbumm(speed, faktor);
    }

    @Override
    public void bearbeiteKleinbumm(final double speed, final double faktor) {
        if (!kleinbummAktiv) {
            if (laser.feuert()) {
                boolean treffer;
                final int[] x = laser.xP();
                final int[] y = laser.yP();
                final int height = bild.getHeight(null);
                final int width = bild.getWidth(null);
                final Rectangle bounds = new Rectangle(pos.getIntX(), pos.getIntY(), width, height);
                for (int i = 0; i < x.length; i++) {
                    if (bounds.contains(x[i], y[i])) {
                        kleinbummAktiv = true;
                        bummBild = bummAni.startBild();
                        bummSpeed = speed * faktor;
                        speedDurchgang = 0;
                        Werte.erhoehePunkteUm(20);
                        bummPunkt = new Punkt(pos.x + width / 2, pos.y + height / 2);
                        destroy();
                        if (Werte.musikAn()) {
                            new Ton("ufoCrash2.wav", true).play();
                        }
                        break;
                    }
                }
            }
        } else {
            bummBild = bummAni.aktuellesBild(5, faktor);
            if (bummAni.kompletterZyklus())
                kleinbummAktiv = false;
        }
    }

    @Override
    public void zeichneKleinbumm(final Graphics2D g) {
        if (kleinbummAktiv) {
            final int height = bummBild.getHeight(null);
            final int width = bummBild.getWidth(null);
            final Punkt p = new Punkt(bummPunkt.x - width / 2, bummPunkt.y - height / 2);
            g.drawImage(bummBild, p.getIntX(), p.getIntY(), null);
        }
    }

}

