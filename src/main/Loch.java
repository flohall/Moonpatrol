package main;

import myLib.Datei;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;

/**
 * Ein Hindernis: Das Loch (2 verschiedene Groessen)
 * pos aus Spielobjekt bezieht sich auf vordere Lochkante
 *
 * @author Florian Hallay
 * @version 02.05.2013
 */
public class Loch extends Hindernis {

    //Konstanten
    public static final int NORMAL = 130;

    public static final int GROSS = 160;

    private static Image staticNormalBild;

    private static Image staticGrossBild;

    private Image bild;

    // Konstruktor
    public Loch(final Auto pAuto, final int pGroesse) {
        super(pAuto, pGroesse);
        //Bilder nur einmal laden
        if (staticNormalBild == null || staticGrossBild == null) {
            staticNormalBild = Datei.leseBild("loch130.gif");
            staticGrossBild = Datei.leseBild("loch160.gif");
        }
        if (groesse == NORMAL)
            bild = staticNormalBild;
        else if (groesse == GROSS)
            bild = staticGrossBild;
        else
            System.err.println(
                    "class Loch NumberFromatException:"
                            + "\n..in constructor [pGroesse] = " + pGroesse
                            + "\n..object " + this);
    }

    @Override
    protected void zeichneTeil(final Graphics2D g) {
        final int xL = pos.getIntX();//links
        final int yL = pos.getIntY();//links

        g.drawImage(bild, xL, yL + 1, null);
        g.drawImage(bild, xL - 1, yL, null);
        g.drawImage(bild, xL + 1, yL, null);
        g.setComposite(AlphaComposite.DstOut);
        g.drawImage(bild, xL, yL, null);
        g.setComposite(AlphaComposite.SrcOver);
    }

    @Override
    protected void berechneTeil(final double faktor) {
    } //tue nichts

    public int xPTiefpunkt() {
        return (int) (pos.x + groesse / 2);
    }

    public int yPTiefpunkt() {
        return (int) (pos.y + groesse / 3);
    }

    @Override
    protected void setzeTreffer() {
        if (yP() <= car.yP() + 5
                && (car.reifen.xP1 > xP() && car.reifen.xP1 < xP() + groesse
                || car.reifen.xP2 > xP() && car.reifen.xP2 < xP() + groesse
                || car.reifen.xP3 > xP() && car.reifen.xP3 < xP() + groesse)) {
            car.setzeGetroffen(true);
            car.setzeTodesHindernis(this);
        }
    }

}
