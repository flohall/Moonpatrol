package main;

import myLib.Punkt;
import myLib.Rechner;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 * Ein Hindernis: Die Mine (1 Groesse)
 * pos aus Spielobjekt bezieht sich auf vordere untere Ecke
 *
 * @author Florian Hallay
 * @version 05.05.2013
 */
public class Mine extends Hindernis {

    private final int hoehe;

    private double durchgang;

    private boolean blink;

    // Konstruktor
    public Mine(final Auto pAuto) {
        super(pAuto, 15);
        hoehe = 4;
    }

    @Override
    protected void zeichneTeil(final Graphics2D g) {
        if (!zerstoert) //wird unsichtbar bei Kollision
        {
            final int x = pos.getIntX();
            final int y = pos.getIntY() - hoehe;

            g.setColor(Color.BLACK);
            g.fillRect(x, y, groesse, hoehe);
            if (blink)
                g.setColor(Color.WHITE);
            else
                g.setColor(Color.RED);
            g.fillRect(x + 3, y + 1, groesse - 6, hoehe - 1);
        }
    }

    @Override
    protected void berechneTeil(final double faktor) {
        blinken(faktor);
    }

    @Override
    protected void setzeTreffer() {
        final double xL = pos.x;
        final double yL = pos.y - hoehe;
        final double xR = pos.x + groesse;
        final double yR = pos.y - hoehe;
        final Punkt l = new Punkt(xL, yL);
        final Punkt r = new Punkt(xR, yR);

        if (xL < car.xP() && xL > car.xP() - car.laenge //nicht notwendige Vorabpruefung (Zeitersparnis)
                && (Rechner.abstand(xL, yL, car.reifen.xP1, car.reifen.yP1) < car.reifen.radius1
                || Rechner.abstand(xL, yL, car.reifen.xP2, car.reifen.yP2) < car.reifen.radius2
                || Rechner.abstand(xL, yL, car.reifen.xP3, car.reifen.yP3) < car.reifen.radius3
                || Rechner.abstand(xR, yR, car.reifen.xP1, car.reifen.yP1) < car.reifen.radius1
                || Rechner.abstand(xR, yR, car.reifen.xP2, car.reifen.yP2) < car.reifen.radius2
                || Rechner.abstand(xR, yR, car.reifen.xP3, car.reifen.yP3) < car.reifen.radius3)) {
            car.setzeGetroffen(true);
            destroy();
        }
    }

    private void blinken(final double faktor) {
        durchgang += faktor;
        if (blink & durchgang >= 5 || !blink & durchgang >= 10) {
            durchgang = 0;
            blink = !blink;
        }
    }

}

