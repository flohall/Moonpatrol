package main;

import myLib.Datei;
import myLib.Logbuch;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Taskbar;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Class MainProgramm
 * Klasse zum ausfuehren von Moon Patrol
 * als Javaanwendung
 *
 * @author Florian Hallay
 * @version 21.04.2013
 */
public class MainApplication {

    /**
     * Start as APPLICATION
     */
    public static void main(final String[] args) {
        Logbuch.info("Neues Spiel - Version " + Werte.VERSION + " Application");
        Werte.setEnvironment(Werte.APPLICATION);
        Werte.setzeStartWerte();
        final MainApplication prog = new MainApplication();
    }

    final MainPanel panel;

    final JFrame fenster;

    double seitenVerhaeltnis;

    final Dimension tempGroesse;

    final java.awt.Container cPane;

    /**
     * Constructor for objects of class MainProgramm
     */
    public MainApplication() {
        fenster = new JFrame();
        cPane = fenster.getContentPane();
        panel = new MainPanel();
        final JDesktopPane desk = new JDesktopPane();
        //panel wird dem ContentPane von fenster hinzugefuegt
        cPane.add(desk);
        desk.add(panel);
        //fenster bekommt Titel
        fenster.setTitle("MoonPatrol " + Werte.VERSION);
        //setSize oder resize zaehlen Fensterrand mit -> zu kleiner ContentPane
        cPane.setPreferredSize(panel.getPreferredSize());
        //automatische Anpassung der Fenstergroesse an die empfohlene groesse des ContentPane
        fenster.pack();
        //die fenstergroesse wird in tempGroesse gespeichert
        tempGroesse = cPane.getSize();
        //System.exit(0) wenn fenster gesclossen wird
        fenster.setDefaultCloseOperation(fenster.EXIT_ON_CLOSE);

        configureAppIcon(fenster);

        //fenster ist jetzt sichtbar
        fenster.setVisible(true);
        fenster.addComponentListener(new Fenstergroessenanpassung());
        fenster.addWindowListener(new Fensterminimierung());
        panel.addKeyListener(new TastaturverwaltungErweiterung());
        updateWindowScale();
    }

    private static void configureAppIcon(final JFrame jFrame) {
        // this is new since JDK 9
        final Taskbar taskbar = Taskbar.getTaskbar();

        final Image image = Datei.leseBild("icon.gif");
        try {
            // set icon for MacOs (and other systems which do support this method)
            taskbar.setIconImage(image);
            taskbar.setIconBadge(Werte.VERSION);
        } catch (final UnsupportedOperationException e) {
            Logbuch.info("The os does not support: 'taskbar.setIconImage'");
        } catch (final SecurityException e) {
            Logbuch.info("There was a security exception for: 'taskbar.setIconImage'");
        }

        // set icon for windows os (and other systems which do support this method)
        jFrame.setIconImage(image);
    }

    private void updateWindowScale() {
        final double scaleX = fenster.getGraphicsConfiguration().getDefaultTransform().getScaleX();
        final double scaleY = fenster.getGraphicsConfiguration().getDefaultTransform().getScaleY();
        panel.setWindowScale(scaleX, scaleY);
    }

    public void fuehreAus() {
        panel.vorbereiten();
        panel.fuehreAus();
    }

    class Fenstergroessenanpassung extends ComponentAdapter {

        @Override
        public void componentResized(final ComponentEvent e) {
            final int height = cPane.getHeight();
            final int width = cPane.getWidth();
            updateWindowScale();
            panel.setSize(width, height);
            cPane.setPreferredSize(new Dimension(width, height));
            fenster.pack();
        }

    }

    class Fensterminimierung extends WindowAdapter {

        @Override
        public void windowIconified(final WindowEvent e) {
            panel.halteAn();
        }

    }

    class TastaturverwaltungErweiterung extends KeyAdapter {

        @Override
        public void keyPressed(final KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_S) {
                final Dimension size = panel.getPreferredSize();
                panel.setSize(size);
                cPane.setPreferredSize(size);
                fenster.pack();
            }
        }

    }

}
