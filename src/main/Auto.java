package main;

import myLib.BildAnimator;
import myLib.Datei;
import myLib.Rechner;
import myLib.sound.Ton;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;

/**
 * Klasse zum Darstellen und Bewegen des Mondfahrzeugs
 * Das Auto Design wird durch Bilddatei "auto.gif" festgelegt
 *
 * @author Florian Hallay
 * @version 02.05.2013
 */
public class Auto extends SpielObjekt {

    // Attribute
    public final int laenge;

    public final int hoehe;

    private final int groesse;

    private double sprungSpeed;

    private int hinher;

    private boolean sollSpringen;

    private boolean springt;

    private boolean faellt;

    private boolean beschleunige;

    private boolean bremse;

    private int sturzWinkel;

    private double startV;

    private boolean getroffen;

    private final Ton sprungTon;

    public final Explosion explosion;

    /*
     * Auto hat 4 Modi
     * 1.unsichtbar und keineAktion
     * 2.sichtbar und keineAktion
     * 3.sichtbar und nebenAktion
     * 4.sichtbar und hauptAktion
     */
    private boolean sichtbar;

    private boolean nebenAktion;

    private boolean hauptAktion;

    // Objekte
    private final Image autoBild;

    public final Reifen reifen;

    public final Laser laser;

    /**
     * Constructor for objects of class Auto
     */
    public Auto() {
        groesse = 1;
        laenge = groesse * 110;
        hoehe = groesse * 35;
        reifen = new Reifen();
        laser = new Laser();
        autoBild = Datei.leseBild("auto.gif");
        sprungTon = new Ton("jump3.wav", false);
        explosion = new Explosion();
        reset();
    }

    /**
     * Reset des Autos
     * Auto f�hrt jetzt wieder von links ins Bild rein
     */
    public void reset() {
        laser.neu();
        reifen.neu();
        explosion.neu();
        pos.x = 0;
        pos.y = 385;
        hinher = 0;
        sprungSpeed = 4;
        sturzWinkel = 0;
        sollSpringen = false;
        springt = false;
        faellt = false;
        hauptAktion = false;
        nebenAktion = true;
        sichtbar = true;
        getroffen = false;
        Werte.setzeHindernisSpeed(Werte.standardHindernisSpeed());
    }

    public void setzeTodesHindernis(final Hindernis pHindernis) {
        explosion.setzeTodesHindernis(pHindernis);
    }

    public void setzeGetroffen(final boolean flag) {
        getroffen = flag;
    }

    public boolean gibGetroffen() {
        return getroffen;
    }

    /**
     * Zeichnen des Autos
     * Grafikobjekt sollte von �bergebenen JPanel sein
     */
    @Override
    public void zeichne(final Graphics2D g) {
        if (sichtbar) {
            final int x = pos.getIntX() - laenge;
            final int y = pos.getIntY() - hoehe;
            /*
             * ACHTUNG: Die Transformation aendert nur die Zeichnung
             * Die Werte in den Variablen wurden nicht angepasst.
             * Der Laser kann zB.: im gedrehten Zustand nur parallel zur x-Achse schie�en
             */
            final AffineTransform transUrsprung = g.getTransform(); //die aktuelle Transformation des Grafikobjekts von SpielPanel ohne Drehung des Autos
            if (!hauptAktion && !nebenAktion)//dreht das Auto
            {
                final AffineTransform trans = new AffineTransform(transUrsprung);
                trans.rotate(Math.toRadians(sturzWinkel), reifen.xP2, reifen.yP2);
                g.setTransform(trans);
            }
            g.drawImage(autoBild, x, y, null);
            reifen.zeichne(g);
            if (!hauptAktion && !nebenAktion)//entdreht das Auto
            {
                g.setTransform(transUrsprung);
            }
        }
        laser.zeichne(g);
        explosion.zeichne(g);
    }

    @Override
    public void berechne(final double faktor) {
        bewege(faktor);
        laser.bewege(xP(), yP(), faktor);
    }

    /**
     * Bewegt das Auto mit Geschwindigkeit 4 pixel pro Durchlauf zum Zielpunkt
     * Wird ben�tigt um in einen Krater zu fahren, berechnet auch den Winkel -> sturzWinkel
     */
    public void bewegeZu(final int zielX, final int zielY, final double faktor) {
        final double speed = 4 * faktor;
        final double startX = pos.x - laenge / 2;
        final double startY = pos.y;
        final double deltaX = startX - zielX;
        final double deltaY = startY - zielY;
        final double m = deltaY / deltaX;
        double neuX = 0;
        double neuY = 0;
        if (m > 0) //Verhindert teilen durch Null //Divide by zero Error nicht moeglich
        {
            neuX = startX + speed / (1 + m);
            neuY = startY + speed / (1 + m) * m;
        } else {
            neuX = startX + speed / (m - 1);
            neuY = startY + speed / (m - 1) * m;
        }
        pos.x = neuX + laenge / 2;
        pos.y = neuY;
        reifen.bearbeite(xP(), yP(), (int) (neuX - startX), false, faktor);
        sturzWinkel = (int) (25 - Rechner.abstand(startY, zielY) / 5);
        sturzWinkel = (int) (sturzWinkel * 1 / (m * 3));
    }

    public void bewegeRunter(final int zielY, final double faktor) {
        if (yP() < zielY)
            pos.y += 2 * faktor;
        if (yP() > zielY)
            pos.y = zielY;
    }

    public void setzeSollSpringen(final boolean p) {
        if (hauptAktion) {
            sollSpringen = p;
        }
    }

    public void setzeSollFeuern(final boolean p) {
        if (hauptAktion)
            laser.setzeSollFeuern(p);
    }

    public boolean hauptAktion() {
        return hauptAktion;
    }

    public void setzeHauptAktion(final boolean p) {
        hauptAktion = p;
        if (hauptAktion) {
            nebenAktion = false;
            sichtbar = true;
        }
    }

    public void setzeSichtbar(final boolean p) {
        sichtbar = p;
        if (!sichtbar) {
            hauptAktion = false;
            nebenAktion = false;
        }
    }

    public void setzeKeineAktion() {
        hauptAktion = false;
        nebenAktion = false;
    }

    public int sturzWinkel() {
        return sturzWinkel;
    }

    public Rectangle getBounds() {
        return new Rectangle(pos.getIntX() - laenge, pos.getIntY() - hoehe, laenge, hoehe);
    }

    public void beschleunige(final boolean flag) {
        beschleunige = flag;
        if (beschleunige)
            bremse = false;
    }

    public void bremse(final boolean flag) {
        bremse = flag;
        if (bremse)
            beschleunige = false;
    }

    private void veraendereSpeed(final double faktor) {
        if (!springt && !faellt) {
            final double altSpeed = Werte.hindernisSpeed();
            if (beschleunige) {
                final double prozent = 100.21;
                double neuSpeed;

                neuSpeed = altSpeed * Math.pow(prozent / 100.0, faktor);

                //begrenzung
                if (neuSpeed > Werte.maxHindernisSpeed())
                    neuSpeed = Werte.maxHindernisSpeed();
                Werte.setzeHindernisSpeed(neuSpeed);
            } else if (bremse) {
                final double prozent = 99.8;
                double neuSpeed;

                neuSpeed = altSpeed * Math.pow(prozent / 100.0, faktor);
                //begrenzung
                if (neuSpeed < Werte.minHindernisSpeed())
                    neuSpeed = Werte.minHindernisSpeed();
                Werte.setzeHindernisSpeed(neuSpeed);
            } else
                normalisiereSpeed(faktor);
            final double deltaSpeed = Werte.hindernisSpeed() - altSpeed;
            final double autoSpeed = deltaSpeed * 100;
            pos.x += autoSpeed;

            if (Werte.hindernisSpeed() != Werte.standardHindernisSpeed()) {
                hinher = 0;
                startV = 0;
                if (!faellt && !springt)
                    reifen.bearbeite(xP(), yP(), autoSpeed, true, faktor);
            }
        }
    }

    private void normalisiereSpeed(final double faktor) {
        final double altSpeed = Werte.hindernisSpeed();
        double neuSpeed = altSpeed;
        final int standardSpeed = Werte.standardHindernisSpeed();
        final double v;
        if (!(altSpeed == standardSpeed)) {
            if (altSpeed < standardSpeed) {
                if (altSpeed >= standardSpeed - 0.01 * faktor)
                    neuSpeed = standardSpeed;
                else {
                    v = standardSpeed - altSpeed;
                    if (startV < v)
                        startV = v;
                    if (v == startV)
                        neuSpeed = altSpeed + 0.01 * faktor;
                    else
                        neuSpeed = altSpeed + (startV - v + 0.01) * v / 20 * faktor;
                }
            } else if (altSpeed > standardSpeed) {
                if (altSpeed <= standardSpeed + 0.01 * faktor)
                    neuSpeed = standardSpeed;
                else {
                    v = altSpeed - standardSpeed;
                    if (startV < v)
                        startV = v;
                    if (v == startV)
                        neuSpeed = altSpeed - 0.01 * faktor;
                    else
                        neuSpeed = altSpeed - (startV - v + 0.01) * v / 20 * faktor;
                }
            }
        }
        Werte.setzeHindernisSpeed(neuSpeed);
    }

    /**
     * Vereint alle Bewegebefehle dieser Klasse bis auf bewegeZu(..)
     * Je nach Automodus werden verschiedene Bewegungen ausgef�hrt
     */
    @Override
    protected void bewege(final double faktor) {
        if (nebenAktion) {
            anfangbewege(faktor);
            if (xP() >= 400)
                setzeHauptAktion(true);
        }
        if (hauptAktion) {
            veraendereSpeed(faktor);
            springe(faktor);
            hinher(faktor);
        }
    }

    /**
     * Anfangsbewegung des Autos vom linken Rahmen zu Standardposition
     */
    private void anfangbewege(final double faktor) {
        final double speed = 4 * faktor;
        pos.x += speed;
        reifen.bearbeite(xP(), yP(), speed, true, faktor);
    }

    /**
     * F�hrt die Sprungbewegung aus wenn m�glich
     */
    private void springe(final double faktor) {
        setzeSprung();
        final double sprungBeschleunFaktor = Math.pow(1.04, faktor);
        if (springt && yP() >= 335) {
            pos.y -= (int) (sprungSpeed * faktor);
            sprungSpeed = sprungSpeed / sprungBeschleunFaktor;
            reifen.bearbeite(xP(), yP(), 3, false, faktor);
        }
        if (yP() <= 335 && springt) {
            springt = false;
            faellt = true;
        }
        if (faellt) {
            pos.y += (int) (sprungSpeed * faktor);
            sprungSpeed = sprungSpeed * sprungBeschleunFaktor;
            reifen.bearbeite(xP(), yP(), 3, false, faktor);
        }
        if (yP() > 385) {
            pos.y = 385;
        }
        if (yP() == 385) {
            sprungSpeed = 4;
            faellt = false;
        }
    }

    /**
     * Reagiert auf sollSpringen
     * wenn sollSpringen true und springen m�glich -> springt=true
     */
    private void setzeSprung() {
        if (springt)
            sollSpringen = false;
        else {
            if (sollSpringen && !faellt) {
                sollSpringen = false;
                springt = true;
                if (Werte.musikAn())
                    sprungTon.play();
            }
        }
    }

    /**
     * Bewegt das Auto abwechselnd ein paar Pixel vor und zur�ck
     * soll eine nicht 100% gleichbleibende Geschwindigkeit simulieren
     */
    private void hinher(final double faktor) {
        if (!springt && !faellt && Werte.hindernisSpeed() == Werte.standardHindernisSpeed()) {
            final double hinherSpeed = 0.25 * faktor;
            if (hinher >= 0 && hinher < 50) {
                pos.x += hinherSpeed;
                hinher++;
                reifen.bearbeite(xP(), yP(), hinherSpeed, true, faktor);
            }
            if (hinher >= 50 && hinher < 100) {
                pos.x -= hinherSpeed;
                hinher++;
                reifen.bearbeite(xP(), yP(), -hinherSpeed, true, faktor);
            }
            if (hinher == 100)
                hinher = 0;
        }
    }

    /**
     * Klasse fuer die Abwicklung bei Zerst�rung des Fahrzeugs(Auto)
     *
     * @author Florian Hallay
     * @version 02.05.2013
     */
    class Explosion {

        private Hindernis todesHindernis;

        private boolean aktiv;

        private boolean explodiert;

        private final BildAnimator ani;

        private Image bild;

        private final Ton bummTon;

        private final Auto car;

        public Explosion() {
            car = Auto.this;
            ani = new BildAnimator("autoExplo", ".gif", 8);
            bummTon = new Ton("autoCrash3.wav", false);
            neu();
        }

        public void neu() {
            todesHindernis = null;
            aktiv = false;
            explodiert = false;
            bild = ani.startBild();
        }

        public void zeichne(final Graphics g1) {
            if (explodiert) {
                final Graphics2D g = (Graphics2D) g1;
                /*
                 * ACHTUNG: Die Transformation aendert nur die Zeichnung
                 * Die Werte in den Variablen wurden nicht angepasst.
                 */
                final AffineTransform transUrsprung = g.getTransform(); //die aktuelle Transformation des Grafikobjekts von SpielPanel ohne Drehung des Autos
                //dreht
                final AffineTransform trans = new AffineTransform(transUrsprung);
                trans.rotate(Math.toRadians(car.sturzWinkel()), car.reifen.xP2, car.reifen.yP2);
                g.setTransform(trans);
                //zeichnet
                g.drawImage(bild, car.xP() - car.laenge - 5, car.yP() - car.hoehe - 5, null);
                //entdreht
                g.setTransform(transUrsprung);
            }
        }

        public void bearbeite(final double faktor) {
            if (!aktiv) {
                aktiv = true;
                explodiert = false;
                car.setzeKeineAktion();
                Werte.setzeMinusLeben();
                bild = ani.startBild();
            }
            if (aktiv) {
                if (todesHindernis instanceof Loch) {
                    final Loch todesLoch = (Loch) todesHindernis;
                    if (car.yP() < todesLoch.yPTiefpunkt()) {
                        car.bewegeZu(todesLoch.xPTiefpunkt(), todesLoch.yPTiefpunkt(), faktor);
                    } else {
                        bild = ani.aktuellesBild(5, faktor);
                        if (!explodiert) {
                            explodiert = true;
                            car.setzeSichtbar(false);
                            if (Werte.musikAn())
                                bummTon.play();
                        }
                    }
                } else {
                    bild = ani.aktuellesBild(5, faktor);
                    if (!explodiert) {
                        explodiert = true;
                        car.setzeSichtbar(false);
                        if (Werte.musikAn())
                            bummTon.play();
                    }
                }
                if (ani.kompletterZyklus()) {
                    aktiv = false;
                    explodiert = false;
                }
            }
        }

        public boolean istAktiv() {
            return aktiv;
        }

        public void setzeTodesHindernis(final Hindernis pHindernis) {
            todesHindernis = pHindernis;
        }

    }

}
