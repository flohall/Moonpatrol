package main;

import main.interfaces.Explosive;
import myLib.BildAnimator;
import myLib.Datei;
import myLib.Punkt;
import myLib.sound.Ton;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Polygon;
import java.awt.Rectangle;

/**
 * @author Florian Hallay
 * @version 05.05.2013
 */
class Bombe implements Explosive {

    private static BildAnimator staticBodenAni;

    private static BildAnimator staticLuftAni;

    private boolean sollFeuern;

    private boolean bereit;

    private Punkt mittelpunkt;

    private double bummSpeed;

    private double seitwaertsDrall;

    private Punkt bummPunkt;

    private boolean luftExplosion;

    private boolean bodenExplosion;

    private boolean zerstoert;

    private double speedDurchgang;

    private final Auto car;

    private final BildAnimator bodenBummAni;

    private Image bodenBummBild;

    private final BildAnimator luftBummAni;

    private Image luftBummBild;

    public Bombe(final Auto pAuto) {
        car = pAuto;
        if (staticBodenAni == null || staticLuftAni == null) {
            staticBodenAni = new BildAnimator("bodenExplosion", ".gif", 8);
            staticLuftAni = new BildAnimator("bombeExplosion", ".gif", 3);
            final Image tempBild = staticLuftAni.startBild();
            Datei.warteAufBild(tempBild);
            final int height = tempBild.getHeight(null);
            final int width = tempBild.getWidth(null);

            final java.awt.image.BufferedImage buff = new java.awt.image.BufferedImage(width, height, java.awt.image.BufferedImage.TYPE_INT_ARGB);
            final Graphics2D buffG = buff.createGraphics();
            buffG.setColor(Color.RED);
            buffG.setFont(new java.awt.Font("SansSerif", java.awt.Font.PLAIN, 18));
            buffG.drawString(100 + "", width / 2 - 16, height / 2 + 7);
            buffG.dispose();

            staticLuftAni.haengeBildAn(buff);
            staticLuftAni.haengeBildAn(buff);
        }
        bodenBummAni = (BildAnimator) staticBodenAni.clone();
        luftBummAni = (BildAnimator) staticLuftAni.clone();
        neu();
    }

    public void neu() {
        destroy();
        zerstoert = false;
        sollFeuern = false;
        bereit = false;
        luftExplosion = false;
        bodenExplosion = false;
        seitwaertsDrall = 0;
        bodenBummBild = bodenBummAni.startBild();
        luftBummBild = luftBummAni.startBild();
        bummPunkt = null;
        speedDurchgang = 0;
    }

    public void destroy() {
        mittelpunkt = new Punkt();
        zerstoert = true;
    }

    public void zeichne(final Graphics2D g) {
        if (sollFeuern && bereit && !zerstoert) {
            final Polygon polygon = polygon();
            g.setColor(Color.MAGENTA);
            g.fill(polygon);
            g.setColor(Color.BLUE);
            g.draw(polygon);
        }
        zeichneKleinbumm(g);
    }

    public void bewege(final double xP, final double yP, final double ufoSpeed, final double faktor) {
        if (!zerstoert) {
            if (sollFeuern && bereit) {
                final double x = speedDurchgang;
                double ySpeed;
                ySpeed = 0.2 * x;
                if (ySpeed > 4.5)
                    ySpeed = 4.5;
                mittelpunkt.y += ySpeed;
                double xSpeed = 0;
                if (seitwaertsDrall < 0) {
                    xSpeed = seitwaertsDrall + 0.3 * x;
                    if (xSpeed > 0.25)
                        xSpeed = 0.25;
                } else {
                    xSpeed = seitwaertsDrall - 0.3 * x;
                    if (xSpeed < -0.25)
                        xSpeed = -0.25;
                }
                mittelpunkt.x += xSpeed;
                setzeCarTreffer();
                speedDurchgang += faktor;
            } else {
                mittelpunkt.x = xP;
                mittelpunkt.y = yP;
                seitwaertsDrall = ufoSpeed;
                bereit = true;
            }
        }
        final double speed = Werte.hindernisSpeed();
        bearbeiteKleinbumm(speed, faktor);
    }

    public void setzeCarTreffer() {
        final Rectangle bounds = car.getBounds();
        if (bounds.contains(mittelpunkt.x, mittelpunkt.y) && !Werte.unbesiegbar()) {
            car.setzeGetroffen(true);
            destroy();
        }
    }

    @Override
    public void bearbeiteKleinbumm(final double speed, final double faktor) {
        bearbeiteBodenExplosion(speed, faktor);
        bearbeiteLuftExplosion(speed, faktor);
    }

    private void bearbeiteLuftExplosion(final double speed, final double faktor) {
        if (!luftExplosion) {
            if (car.laser.feuert()) {
                final int[] x = car.laser.xP();
                final int[] y = car.laser.yP();
                final Polygon polygon = polygon();
                for (int i = 0; i < x.length; i++) {
                    if (polygon.contains(x[i], y[i])) {
                        //                         kleinbummAktiv=true;
                        //                         durchgang=0;
                        Werte.erhoehePunkteUm(100);
                        //                         bummPunkt = new Punkt(pos.x+width/2, pos.y+height/2);
                        luftExplosion = true;
                        bummPunkt = mittelpunkt;
                        destroy();
                        if (Werte.musikAn()) {
                            new Ton("bombcrash.wav", true).play();
                        }
                        break;
                    }
                }
            }
        } else {
            luftBummBild = luftBummAni.aktuellesBild(5, faktor);
            if (bummSpeed < speed)
                bummSpeed *= 1.06;
            bummPunkt.x -= bummSpeed;
            if (luftBummAni.kompletterZyklus()) {
                neu();
                luftExplosion = false;
            }
        }
    }

    private void bearbeiteBodenExplosion(final double speed, final double faktor) {
        if (!bodenExplosion) {
            if (mittelpunkt.y > Werte.BODENHOEHE) {
                bodenExplosion = true;
                bummPunkt = new Punkt(mittelpunkt.x, Werte.BODENHOEHE);
                destroy();
                if (Werte.musikAn()) {
                    new Ton("bodencrash4.wav", true).play();
                }
            }
        } else {
            bodenBummBild = bodenBummAni.aktuellesBild(5, faktor);
            bummPunkt.x -= speed;
            if (bodenBummAni.kompletterZyklus()) {
                neu();
                bodenExplosion = false;
            }
        }
    }

    @Override
    public void zeichneKleinbumm(final Graphics2D g) {
        if (bodenExplosion) {
            final int height = bodenBummBild.getHeight(null);
            final int width = bodenBummBild.getWidth(null);
            final Punkt p = new Punkt(bummPunkt.x - width / 2, bummPunkt.y - height);
            g.drawImage(bodenBummBild, p.getIntX(), p.getIntY(), null);
        } else if (luftExplosion) {
            final int height = luftBummBild.getHeight(null);
            final int width = luftBummBild.getWidth(null);
            final Punkt p = new Punkt(bummPunkt.x - width / 2, bummPunkt.y - height / 2);
            g.drawImage(luftBummBild, p.getIntX(), p.getIntY(), null);
        }
    }

    public void setzeSollFeuern(final boolean p) {
        if (car.hauptAktion())
            sollFeuern = p;
    }

    public boolean feuert() {
        return sollFeuern && bereit;
    }

    private Polygon polygon() {
        final int xP = mittelpunkt.getIntX();
        final int yP = mittelpunkt.getIntY();
        final int[] x = {xP - 8, xP, xP + 8};
        final int[] y = {yP - 9, yP, yP - 9};
        return new Polygon(x, y, 3);
    }

}
