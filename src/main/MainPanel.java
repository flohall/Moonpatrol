package main;

import main.tests.Control;
import main.tests.Terminal;
import myLib.Datei;
import myLib.Logbuch;
import myLib.Verbindung;
import myLib.sound.Ton;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.OverlayLayout;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.text.html.HTMLEditorKit;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.geom.AffineTransform;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Class MainPanel
 * Klasse die das gesamte Spiel verwaltet und auf einen JPanel darstellt
 *
 * @author Florian Hallay
 * @version 11.05.2013
 */
public class MainPanel extends JPanel implements Runnable {

    private boolean loop;

    private static final int INFOPANELHEIGHT = 50;

    public final Spielverwaltung game;

    private final SpielPanel spielPanel;

    private Terminal terminal;

    private final MenuPanel introPanel;

    public final Pause pause;

    private final Ton musik;

    private Thread spielThread;

    private final Knopfverwaltung knopfverwaltung;

    private final AffineTransform trans;

    private final CardLayout cards;

    private double windowScaleX = 1;

    private double windowScaleY = 1;

    public MainPanel() {
        cards = new CardLayout();
        setLayout(cards);
        addKeyListener(new Tastaturverwaltung());
        setPreferredSize(new Dimension(1000, 500));
        setSize(getPreferredSize());
        setBorder(javax.swing.BorderFactory.createLineBorder(Color.BLACK));
        //ActionListener fuer Knoepfe
        knopfverwaltung = new Knopfverwaltung();

        spielPanel = new SpielPanel();

        introPanel = new MenuPanel();
        pause = new Pause();
        musik = new Ton("musik.mid", false);
        game = new Spielverwaltung(this);
        trans = new AffineTransform();
        /*
         * ACHTUNG Adding-Reihenfolge beachten // add(spiel) immer zuletzt
         */
        setBackground(Color.BLUE);
        add(introPanel, "intro");
        add(spielPanel, "spiel");
        repaint();
        setFocusable(true);
    }

    public void setWindowScale(final double scaleX, final double scaleY) {
        windowScaleX = scaleX;
        windowScaleY = scaleY;
        trans.setToScale(windowScaleX, windowScaleY);
    }

    @Override
    public void setSize(final int width, final int height) {
        super.setSize(width, height);
        if (spielPanel != null)
            spielPanel.setSize(width, height);
    }

    @Override
    public void setSize(final Dimension groesse) {
        setSize((int) groesse.getWidth(), (int) groesse.getHeight());
    }

    public void vorbereiten() {
        if (introPanel.musikKnopf.angeschaltet()) {
            spielPanel.musikKnopf.setzeIconAn();
        } else {
            spielPanel.musikKnopf.setzeIconAus();
        }
        repaint();
    }

    public void fuehreAus() {
        loop = true;
        pause.weiter();
        spielThread = new Thread(this);
        //notwendig wenn defaultEHandler in Werte nicht funktioniert
        spielThread.setUncaughtExceptionHandler(Logbuch.getUncaughtEHandler());
        spielThread.setName("SpielThread");
        spielThread.start();
        if (Werte.musikAn() && !musik.laeuft())
            musik.loop();
    }

    public void halteAn() {
        loop = false;
        pause.pausiere(Pause.NORMAL);
        game.beschleunige(false);
        game.bremse(false);
        musik.stop();
        repaint();
    }

    public void halteAn(final int pausenArt) {
        loop = false;
        pause.pausiere(pausenArt);
        game.beschleunige(false);
        game.bremse(false);
        musik.stop();
        repaint();
    }

    public void beende() {
        halteAn();
        spielThread = null;
        musik.stop();
        game.komplettReset();
    }

    @Override
    public void run() {
        int i = 0;
        while (loop) {
            i++;
            game.berechne(1);
            if (i >= 2) {
                repaint(Werte.sleepTime());
                i = 0;
            }
            try {
                Thread.sleep(Werte.sleepTime());
            } catch (final InterruptedException e) {
            } catch (final IllegalArgumentException e) {
            }
        }
    }

    public void highscoreAbfrage() {
        SwingUtilities.invokeLater(new InformationFrameDialog());
    }

    /**
     * SPIELPANEL
     */
    class SpielPanel extends JPanel {

        final JPanel infoPanel;

        final Knopf musikKnopf;

        final Knopf rekordKnopf;

        final Knopf menuKnopf;

        final Control control;

        public SpielPanel() {
            setLayout(new java.awt.BorderLayout());
            setFocusable(false);
            final JPanel untereEbene = new JPanel() {
                @Override
                public void paintComponent(final Graphics g1) {
                    final Graphics2D g = (Graphics2D) g1;
                    final AffineTransform tempTrans = g.getTransform();
                    g.setTransform(trans);
                    final java.awt.image.BufferedImage buff = new java.awt.image.BufferedImage(Werte.WIDTH, Werte.HEIGHT, java.awt.image.BufferedImage.TYPE_INT_ARGB);
                    final Graphics2D buffG = buff.createGraphics();

                    game.zeichneUntereEbene(buffG);
                    buffG.dispose();
                    g.drawImage(buff, null, 0, 0);

                    g.setTransform(tempTrans);
                }
            };
            final JPanel obereEbene = new JPanel() {
                @Override
                public void paintComponent(final Graphics g1) {
                    final Graphics2D g = (Graphics2D) g1;
                    final AffineTransform tempTrans = g.getTransform();
                    g.setTransform(trans);
                    super.paintComponent(g);

                    final java.awt.image.BufferedImage buff = new java.awt.image.BufferedImage(Werte.WIDTH, Werte.HEIGHT, java.awt.image.BufferedImage.TYPE_INT_ARGB);
                    final Graphics2D buffG = buff.createGraphics();

                    game.zeichneObereEbene(buffG);
                    pause.zeichne(buffG);
                    buffG.dispose();
                    g.drawImage(buff, null, 0, 0);

                    g.setTransform(tempTrans);
                }
            };
            obereEbene.setOpaque(false);
            final JPanel zeichnungsBereich = new JPanel();

            terminal = new Terminal(MainPanel.this);
            control = new Control(MainPanel.this);
            zeichnungsBereich.add(control);
            zeichnungsBereich.add(terminal);
            zeichnungsBereich.add(obereEbene);
            zeichnungsBereich.add(untereEbene);
            zeichnungsBereich.setLayout(new OverlayLayout(zeichnungsBereich));
            add(zeichnungsBereich, "Center");
            infoPanel = new JPanel();
            infoPanel.setOpaque(true);
            infoPanel.setBackground(Color.white);
            infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.LINE_AXIS));
            final JPanel knopfPanel = new JPanel();
            knopfPanel.setLayout(new BoxLayout(knopfPanel, BoxLayout.X_AXIS));

            musikKnopf = new Knopf(Datei.leseIcon("musikAn.gif"), Datei.leseIcon("musikAus.gif"));
            rekordKnopf = new Knopf("Reset Record");
            menuKnopf = new Knopf("Menu");
            final JPanel textPanel = new JPanel() {
                @Override
                public void paintComponent(final Graphics g) {
                    final Font fontStandard = g.getFont();
                    final Font fontBig = g.getFont().deriveFont(15f);

                    g.setColor(Color.BLACK);
                    g.setFont(fontBig);
                    g.drawString("Help:", 100, 20);
                    g.drawString("'p' to pause", 140, 20);
                    g.drawString("SPACE to jump", 260, 20);
                    g.drawString("'r' to shoot", 140, 40);
                    g.drawString("ARROWS to control speed", 260, 40);
                    g.setFont(fontStandard);
                    g.drawString("Points:", 10, 20);
                    g.drawString("Record:", 10, 30);
                    g.drawString("Life:", 10, 40);
                    g.drawString("" + Werte.punkte(), 55, 20);
                    if (Werte.punkte() == Werte.rekord())
                        g.setColor(Color.GREEN);
                    g.drawString("" + Werte.rekord(), 55, 30);
                    g.setColor(Color.BLACK);
                    if (Werte.leben() == 1 || Werte.leben() == 0)
                        g.setColor(Color.RED);
                    g.drawString("" + Werte.leben(), 55, 40);
                }
            };
            textPanel.setPreferredSize(new Dimension(450, INFOPANELHEIGHT));
            textPanel.setMinimumSize(new Dimension(80, INFOPANELHEIGHT));
            infoPanel.add(textPanel);

            knopfPanel.add(musikKnopf);
            knopfPanel.add(Box.createRigidArea(new Dimension(5, 0)));
            knopfPanel.add(rekordKnopf);
            knopfPanel.add(Box.createRigidArea(new Dimension(5, 0)));
            knopfPanel.add(menuKnopf);
            knopfPanel.setOpaque(false);
            infoPanel.add(knopfPanel);
            infoPanel.add(Box.createHorizontalGlue());
            final JLabel versionLabel = new JLabel(Werte.VERSION);
            versionLabel.setVerticalAlignment(SwingConstants.BOTTOM);
            versionLabel.setHorizontalAlignment(SwingConstants.RIGHT);
            versionLabel.setFont(versionLabel.getFont().deriveFont(10f));
            final JPanel versionPanel = new JPanel();
            versionPanel.setOpaque(false);
            versionPanel.setLayout(new java.awt.BorderLayout());
            versionPanel.add(versionLabel, "East");
            infoPanel.add(versionPanel);

            add(infoPanel, "South");
        }

        @Override
        public void setSize(final int width, final int height) {
            //             super.setSize(width, height);
            try {
                final double xScale = width / Double.valueOf(Werte.WIDTH);
                final double yScale = (height - 50) / Double.valueOf(Werte.HEIGHT);
                trans.setToScale(xScale * windowScaleX, yScale * windowScaleY);
            } catch (final NullPointerException e) {
            }//kein fehler
        }

        @Override
        public void setSize(final Dimension groesse) {
            setSize((int) groesse.getWidth(), (int) groesse.getHeight());
        }

    }

    /**
     * MENUPANEL
     */
    class MenuPanel extends JPanel implements Runnable {

        JButton startButton;

        Knopf musikKnopf;

        JTextField nameFeld;

        public MenuPanel() {
            SwingUtilities.invokeLater(this);
        }

        @Override
        public void run() {
            setFocusable(false);
            setLayout(new java.awt.BorderLayout());
            /*
             * Ueberschrift --> North
             */
            final JLabel ueberschrift = new JLabel("MOON PATROL", SwingConstants.CENTER);
            ueberschrift.setFocusable(false);
            ueberschrift.setFont(ueberschrift.getFont().deriveFont(50f));
            ueberschrift.setBorder(javax.swing.BorderFactory.createEmptyBorder(10, 10, 10, 10));
            ueberschrift.setOpaque(true);
            ueberschrift.setBackground(Color.gray);
            add(ueberschrift, "North");

            /*
             * CenterPane --> Center
             */
            final JPanel centerPane = new JPanel();
            centerPane.setFocusable(false);
            centerPane.setOpaque(true);
            centerPane.setBackground(Color.WHITE);
            centerPane.setLayout(new java.awt.GridLayout());
            final JTextArea area1 = new JTextArea(
                    "Controls: \n"
                            + "  'SPACE' -> jump\n"
                            + "  'R' -> shoot\n"
                            + "  'RIGHTARROW' -> speedup\n"
                            + "  'LEFTARROW' -> slow down\n"
                            + "  'P' -> pause\n"
                            + "  'S' -> standard size\n");
            area1.setFocusable(false);
            area1.setFont(area1.getFont().deriveFont(20f));
            area1.setBorder(javax.swing.BorderFactory.createEmptyBorder(10, 10, 10, 10));
            area1.setEditable(false);
            centerPane.add(area1);
            final JTextArea area2 = new JTextArea(
                    "  Programming and Design\n"
                            + "     Florian Hallay\n"
                            + "  Music\n"
                            + "     Ricardo Feldmann\n"
                            + "\n\n\n\n\n\n"
                            + "  Visit \"" + Datenverwaltung.urlHomepage().replaceAll("http://", "") + "\" for further information\n"
                            + "  have a look at the highscores!\n");
            area2.setFocusable(false);
            area2.setFont(area1.getFont().deriveFont(20f));
            area2.setBorder(javax.swing.BorderFactory.createEmptyBorder(10, 10, 10, 10));
            area2.setEditable(false);
            centerPane.add(area2);
            add(centerPane, "Center");
            /*
             * Fussleiste --> South
             */
            final JPanel fussleiste = new JPanel();
            fussleiste.setFocusable(false);
            fussleiste.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));
            fussleiste.setBorder(javax.swing.BorderFactory.createEmptyBorder(10, 10, 10, 10));
            fussleiste.setOpaque(true);
            fussleiste.setBackground(Color.gray);

            //             JLabel name = new JLabel("Name:");
            //             name.setFont(name.getFont().deriveFont(20f));
            //             fussleiste.add(name);

            //             nameFeld = new JTextField("");
            //             nameFeld.setFocusable(true);
            //             nameFeld.requestFocus();
            //             nameFeld.setPreferredSize(new Dimension(200, 40));
            //             nameFeld.setFont(nameFeld.getFont().deriveFont(20f));
            //             fussleiste.add(nameFeld);

            musikKnopf = new Knopf(Datei.leseIcon("musikAn.gif"), Datei.leseIcon("musikAus.gif"));
            musikKnopf.setPreferredSize(new Dimension(40, 40));
            musikKnopf.setzeIconAn();
            fussleiste.add(musikKnopf);

            startButton = new JButton("Start");
            startButton.setFocusable(true);
            startButton.setFont(startButton.getFont().deriveFont(20f));
            startButton.setPreferredSize(new Dimension(100, 40));
            startButton.addActionListener(knopfverwaltung);
            fussleiste.add(startButton);

            add(fussleiste, "South");
        }

    }

    class Knopf extends JButton {

        private Icon iconAn, iconAus;

        private boolean angeschaltet;

        public Knopf(final String name) {
            super(name);
            setFocusable(false);
            addActionListener(knopfverwaltung);
            angeschaltet = false;
        }

        public Knopf(final Icon iconAn, final Icon iconAus) {
            super(iconAus);
            setFocusable(false);
            addActionListener(knopfverwaltung);
            this.iconAn = iconAn;
            this.iconAus = iconAus;
            angeschaltet = false;
        }

        public void setzeIconAus() {
            setIcon(iconAus);
            angeschaltet = false;
            Werte.setzeMusikAn(false);
        }

        public void setzeIconAn() {
            Werte.setzeMusikAn(true);
            setIcon(iconAn);
            angeschaltet = true;
        }

        public boolean angeschaltet() {
            return angeschaltet;
        }

    }

    /**
     * Klasse die bestimmt was Tastatureingaben bewirken.
     * Muss einem KeyListener uebergeben werden.
     * (implements KeyListener auch moeglich
     * benoetigt aber leere Methoden)
     */
    class Tastaturverwaltung extends KeyAdapter {

        public Tastaturverwaltung() {
        }

        @Override
        public void keyPressed(final KeyEvent e) {
            if (spielPanel.isVisible()) {
                if (Werte.adminRechte()) {
                    /*
                     * Admin Funktionen
                     */
                    switch (e.getKeyCode()) {
                        //verringere Geschwindigkeit
                        case KeyEvent.VK_MINUS:
                            Werte.setzeFps(Werte.gibFps() - 2);
                            break;
                        //erhoehe Geschwindigkeit
                        case KeyEvent.VK_PLUS:
                            Werte.setzeFps(Werte.gibFps() + 2);
                            break;
                    }
                }
                /*
                 * Normale Funktionen & Kontrollfenster anzeigen
                 */
                switch (e.getKeyCode()) {
                    //Pause
                    case KeyEvent.VK_P:
                        if (loop)
                            halteAn();
                        else if (pause.pausiert(Pause.NORMAL))
                            fuehreAus();
                        break;
                    //Auto soll Laser abfeuern
                    case KeyEvent.VK_R:
                        game.autoSollSchiessen();
                        break;
                    //Auto soll springen
                    case KeyEvent.VK_SPACE:
                        game.autoSollSpringen();

                        break;
                    //beschleunigen
                    case KeyEvent.VK_RIGHT:
                        game.beschleunige(true);
                        break;
                    //bremsen
                    case KeyEvent.VK_LEFT:
                        game.bremse(true);
                        break;

                    case KeyEvent.VK_C:
                        if (pause.pausiert(Pause.GAMEOVER)) {
                            game.continueGame();
                            fuehreAus();
                        }
                        break;

                    case KeyEvent.VK_N:
                        if (pause.pausiert(Pause.GAMEOVER)) {
                            game.newGame();
                            fuehreAus();
                        }
                        break;
                    //zeige Informationen (ControlPanel)
                    case KeyEvent.VK_DEAD_CIRCUMFLEX:
                        //dieser KeyKistener ist nicht mehr aktiv deshalb
                        //muessen beschleunigen und bremsen abgebrochen werden
                        if (!pause.pausiert(Pause.GAMEOVER)) {
                            game.beschleunige(false);
                            game.bremse(false);
                            terminal.aktiviere(true);
                            e.consume();
                        }
                        break;
                }
                //                 repaint();
            }
        }

        @Override
        public void keyReleased(final KeyEvent e) {
            switch (e.getKeyCode()) {
                //beschleunigen abbrechen
                case KeyEvent.VK_RIGHT:
                    game.beschleunige(false);
                    break;
                //bremsen abbrechen
                case KeyEvent.VK_LEFT:
                    game.bremse(false);
                    break;
            }
            //             repaint();
        }

    }

    class Knopfverwaltung implements ActionListener {

        @Override
        public void actionPerformed(final ActionEvent actionEvent) {
            if (actionEvent.getSource() == spielPanel.musikKnopf) {
                if (spielPanel.musikKnopf.angeschaltet()) {
                    musik.stop();
                    spielPanel.musikKnopf.setzeIconAus();
                } else {
                    spielPanel.musikKnopf.setzeIconAn();
                    if (!Werte.getPauseAktiv())
                        musik.loop();
                }
            }
            if (actionEvent.getSource() == spielPanel.rekordKnopf) {
                Werte.reseteRekord();
            }
            if (actionEvent.getSource() == spielPanel.menuKnopf) {
                halteAn();
                if (JOptionPane.OK_OPTION == JOptionPane.showInternalConfirmDialog(
                        getParent(), "Exit without saving?", "", JOptionPane.OK_CANCEL_OPTION)) {
                    beende();
                    cards.show(MainPanel.this, "intro");
                } else
                    fuehreAus();
            }
            if (actionEvent.getSource() == introPanel.startButton) {
                //String name=introPanel.nameFeld.getText();
                //if(name.equals(""))
                //    name="[NoName]";
                //Werte.setzeSpielerName(name);
                cards.show(MainPanel.this, "spiel");
                vorbereiten();
                fuehreAus();
            }
            if (actionEvent.getSource() == introPanel.musikKnopf) {
                if (introPanel.musikKnopf.angeschaltet)
                    introPanel.musikKnopf.setzeIconAus();
                else
                    introPanel.musikKnopf.setzeIconAn();
            }
            repaint();
        }

    }

    class InformationFrameDialog implements Runnable {

        @Override
        public void run() {
            halteAn(Pause.GAMEOVER);
            String nickname = null;
            if (Werte.punkte() == Werte.rekord() && !(Werte.punkte() == 0)) {
                nickname = JOptionPane.showInternalInputDialog(getParent(), "Nickname:", "Online-Highscore", JOptionPane.QUESTION_MESSAGE);
                if (nickname != null)
                    SwingUtilities.invokeLater(new InformationFrameDialog2(nickname));
            }
            repaint();
        }

    }

    class InformationFrameDialog2 implements Runnable {

        final String nickname;

        public InformationFrameDialog2(final String nickname) {
            this.nickname = nickname;
        }

        @Override
        public void run() {
            final JTextPane area = new JTextPane();
            area.setContentType("text/html");
            area.setFont(new Font("Courier New", 0, 12));
            area.setEditable(false);
            area.setEditorKit(new HTMLEditorKit());
            area.setText("connect...");
            area.setCaretPosition(0);

            final JScrollPane pane = new JScrollPane(area);
            final JOptionPane oPane = new JOptionPane(pane, JOptionPane.PLAIN_MESSAGE);
            oPane.addPropertyChangeListener(new PropertyChangeListener() {
                @Override
                public void propertyChange(final PropertyChangeEvent e) {
                    repaint();
                    requestFocus();
                }
            });
            final JInternalFrame frame = oPane.createInternalFrame(getParent(), "Highscore");
            frame.addComponentListener(new ComponentAdapter() {
                @Override
                public void componentMoved(final ComponentEvent e) {
                    repaint();
                }
            });
            final Dimension ebenenSize = spielPanel.getSize();
            if (ebenenSize.height - INFOPANELHEIGHT > 420)
                frame.setPreferredSize(new Dimension(360, 420));
            else if (ebenenSize.height - INFOPANELHEIGHT > 220)
                frame.setPreferredSize(new Dimension(360, ebenenSize.height - INFOPANELHEIGHT));
            else
                frame.setPreferredSize(new Dimension(360, 220));
            int xPos = 0;
            int yPos = 0;
            //Mittig zentriet auf den Spielebenen
            xPos = ebenenSize.width / 2 - frame.getPreferredSize().width / 2;
            yPos = (ebenenSize.height - INFOPANELHEIGHT) / 2 - frame.getPreferredSize().height / 2;
            if (xPos < 0)
                xPos = 0;
            if (yPos < 0)
                yPos = 0;
            frame.setLocation(xPos, yPos);
            frame.pack();
            frame.show();
            SwingUtilities.invokeLater(new InformationFrameDialog3(area, nickname));
        }

    }

    class InformationFrameDialog3 implements Runnable {

        final JTextPane area;

        final String nickname;

        public InformationFrameDialog3(final JTextPane area, final String nickname) {
            this.area = area;
            this.nickname = nickname;
        }

        @Override
        public void run() {

            if (nickname != null) {
                final Verbindung verbindung = Verbindung.getVerbindung();
                verbindung.addPostParam("name", nickname);
                verbindung.addPostParam("punkte", Werte.rekord() + "");
                verbindung.addPostParam("version", Werte.VERSION);
                verbindung.sendeViaPost(Datenverwaltung.urlFuerSchreiben());
            }
            String antwort = null;
            antwort = Verbindung.empfangeVonUrl(Datenverwaltung.urlFuerLesen());
            if (antwort != null) {
                area.setText(antwort);
                area.setCaretPosition(0);
            } else {
                area.setText("Failure - 404 Not Found - no internet connection?");
                area.setCaretPosition(0);
            }
            repaint();
        }

    }

}
