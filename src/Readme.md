# Moon Patrol

## Purpose:

- game/fun

## Current version

- 4.5.0
- created: 2023-02-12

## Dependencies

- min. java 9
- tested on java 17 only

## How to run

- Application:
    - run from source:
        - run main in MainApplication
    - run from jar
        - run the jar ()
- Applet:
    - functionality has been removed
- WebStart:
    - functionality has been removed

## Author

- programming, design and sounds: Florian Hallay
- main music: Ricardo Feldmann

## Backgrounds:

- This is a reimplementation of the original game with same name by Irem. But there are many differences.

## For debugging:

- press '^' during the game

## Changelog

### 4.5.0

- development version changed to java 17, functions of java 9 have been used
- changed to semantic version x.x.x
- README now as markdown and in english
- added doc icon for macOs
- fixed issues on retina displays
