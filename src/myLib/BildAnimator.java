package myLib;

import java.awt.Image;

/**
 * Klasse zum Animieren von Bildern
 *
 * @author Florian Hallay
 * @version 21.04.2013
 */
public class BildAnimator implements Cloneable {

    // instance variables - replace the example below with your own
    Image[] images;

    double durchgang;

    int imagesIndex;

    boolean kompletterZyklus;

    boolean loop;

    /**
     * Konstruktor fuer schon geladene Bilder
     * wird fuer clone() benoetigt
     */
    public BildAnimator(final Image[] images) {
        final int length = images.length;
        this.images = new Image[length];
        System.arraycopy(images, 0, this.images, 0, length);
        durchgang = 0;
        imagesIndex = 0;
        loop = true;
        kompletterZyklus = false;
    }

    /**
     * Konstruktor bei beliebigen Dateinamen der Animationsfiles
     */
    public BildAnimator(final String[] bildURLs) {
        final int length = bildURLs.length;
        images = new Image[length];
        for (int i = 0; i < length; i++)
            images[i] = Datei.leseBild(bildURLs[i]);
        durchgang = 0;
        imagesIndex = 0;
        loop = true;
        kompletterZyklus = false;
    }

    /**
     * Konstruktor bei Animationsfiles mit Pfad:
     * [folderName]/[folderName]_frame_[numberFormat(XXXX)][fileExtensions]
     */
    public BildAnimator(final String folderName, String fileExtensions, final int frameAnzahl) {
        final int length = frameAnzahl;
        images = new Image[length];
        final java.text.DecimalFormat nft = new java.text.DecimalFormat("#0000.###");
        if (!fileExtensions.startsWith("."))
            fileExtensions = "." + fileExtensions;
        for (int i = 0; i < length; i++)
            images[i] = Datei.leseBild(folderName + "/" + folderName + "_frame_" + nft.format(i + 1) + fileExtensions);
        durchgang = 0;
        imagesIndex = 0;
        loop = true;
        kompletterZyklus = false;
    }

    /**
     * Es wird ein Object(BildAniamtor) zurueckgegeben,
     * welches die gleichen Bilder enthaelt
     */
    @Override
    public Object clone() {
        return new BildAnimator(images);
    }

    /**
     * Die Animation beginnt jetzt wieder beim ersten Bild
     * das erste Bild wird zurueckgegeben
     */
    public Image startBild() {
        imagesIndex = 0;
        kompletterZyklus = false;
        return images[imagesIndex];
    }

    /**
     * haengt ein Bild an die Animation an
     * dieses ist jetzt das letzte Bild
     */
    public void haengeBildAn(final Image bild) {
        final Image[] tempImages = new Image[images.length + 1];
        System.arraycopy(images, 0, tempImages, 0, images.length);
        tempImages[tempImages.length - 1] = bild;
        images = tempImages;
    }

    /**
     * wechselt die geladenen Bilder
     * und gibt das aktuelle zurueck
     * 'wait' gibt die Anzahl der Aufrufe dieser Methode an,
     * in denen das Bild nicht gewechselt wird
     * 'wait' muss positiv sein ('wait'>0)
     * 0 bedeutet groesste geschwindigkeit
     */
    public Image aktuellesBild(final int wait, final double faktor) {
        if (durchgang >= wait) {
            if (imagesIndex == images.length - 1)
                kompletterZyklus = true;
            durchgang = 0;
            imagesIndex++;
            if (imagesIndex >= images.length) {
                if (loop)
                    imagesIndex = 0;
                else
                    imagesIndex--;
            }
        }
        durchgang += faktor;
        return images[imagesIndex];
    }

    //     /**
    //      * wechselt die geladenen Bilder mit standardspeed
    //      */
    //     public Image aktuellesBild(double faktor)
    //     {
    //         return this.aktuellesBild(5, faktor); //standard speed
    //     }

    /**
     * gibt an ob seit dem letzten Aufruf von startBild
     * alle Bilder der animation bereits gezeigt wurden
     */
    public boolean kompletterZyklus() {
        return kompletterZyklus;
    }

    public void setzeLoop(final boolean flag) {
        loop = flag;
    }

    public int anzahlBilder() {
        return images.length;
    }

}
