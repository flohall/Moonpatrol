package myLib;

import java.awt.Container;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;

/**
 * Ein Rechner der die Funktionalitaeten von java.lang.Math erweitert
 *
 * @author Florian Hallay
 * @version 06.12.2012
 */
public class Rechner {

    public static Vektor addiere(final Vektor v1, final Vektor v2) {
        return new Vektor(v1.x + v2.x, v1.y + v2.y);
    }

    /**
     * Berechne ganzrationale Funktionen
     * Beispiel: ax^2+bx+c
     * abc kann auch mehr oder weniger Buchstaben enthalten als a,b,c
     */
    public static double fVonX(final double[] abc, final double x) {
        int l = 0;
        if (abc != null)
            l = abc.length;
        double ausgabe = 0;
        for (int i = 0; i < l; i++) {
            ausgabe += abc[i] * Math.pow(x, l - i - 1);
        }
        return ausgabe;
    }

    public static double zufallszahl(final double min, final double max) {
        return Math.random() * (max - min) + min;
    }

    public static double abstand(final int x1, final int y1, final int x2, final int y2) {
        return Double.valueOf(Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2)));
    }

    public static double abstand(final double x1, final double y1, final double x2, final double y2) {
        return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    }

    public static double abstand(final Punkt p1, final Punkt p2) {
        return Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2));
    }

    /**
     * Prueft ob Punkt 'p' in oder auf einem Kreis liegt
     * Kreis wird durch radius und mittelPunkt definiert
     */
    public static boolean inKreis(final Punkt p, final Punkt mittelPunkt, final int radius) {
        return abstand(p, mittelPunkt) <= radius;
    }

    public static double abstand(final double x1, final double x2) {
        return Math.sqrt(Math.pow(x2 - x1, 2));
    }

    /**
     * Transformiert den 'container' mit 'trans',
     * 'standardBounds' ist die urspr�ngliche Position und Groesse von 'container',
     * f�r trans sind nur die folgenden Typen erlaubt:
     * TRANSLATION, UNIFORMSCALE, GENERALSCALE, IDENTITY,
     * es wird nichts zur�ckgegeben,
     * 'container' ist nun der Transformierte Container.
     */
    public static void transformiere(final Container container, final Rectangle standardBounds, final AffineTransform trans) {
        final int type = trans.getType();
        double height = standardBounds.getHeight();
        double width = standardBounds.getWidth();
        double xP = standardBounds.getX();
        double yP = standardBounds.getY();
        //ueberprueft ob AffineTransform vom richtigem typ ist
        //bei falschen Typ koennen grobe fehler bei der Darstellungen der Container auftreten
        //nur Verschiebung und Scalierung sind hier gueltig
        if (type == AffineTransform.TYPE_TRANSLATION
                || type == AffineTransform.TYPE_UNIFORM_SCALE
                || type == AffineTransform.TYPE_GENERAL_SCALE
                || type == AffineTransform.TYPE_TRANSLATION + AffineTransform.TYPE_UNIFORM_SCALE
                || type == AffineTransform.TYPE_TRANSLATION + AffineTransform.TYPE_GENERAL_SCALE
                || type == AffineTransform.TYPE_IDENTITY) {
            xP = xP * trans.getScaleX() + trans.getTranslateX();
            yP = yP * trans.getScaleY() + trans.getTranslateY();
            width = width * trans.getScaleX();
            height = height * trans.getScaleY();
            container.setBounds((int) xP, (int) yP, (int) width, (int) height);
        } else
            System.err.println("AffineTransformation vom falschen Typ -> Fehler bei " + container);
    }

}
