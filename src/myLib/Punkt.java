package myLib;

import java.awt.geom.Point2D;

/**
 * Diese Klasse erweitert die Funktionalitaeten von Point2D.Double
 *
 * @author Florian Hallay
 * @version 19.10.2012
 */
public class Punkt extends Point2D.Double {

    public Punkt(final double x, final double y) {
        super(x, y);
    }

    public Punkt(final int x, final int y) {
        super(x, y);
    }

    public Punkt() {
    }

    public int getIntX() {
        return (int) x;
    }

    public int getIntY() {
        return (int) y;
    }

}
