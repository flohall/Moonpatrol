package myLib;

/**
 * Eine Klasse zum verschluessseln von Strings.
 * Verschluesselte Strings koennen auch wieder entschluesselt werden.
 *
 * @author Florian Hallay
 * @version 19.10.2012
 */
public class Schluessel {

    private static final char[][] schluessel =
            {
                    {'J', 'I', 'u', 'v', 'w', 'x', 'f', 'e', 'd'},
                    {'K', 't', 'H', '0', ' ', 'g', 'y', 'z', 'c'},
                    {'L', 's', '1', 'G', 'h', '.', ',', 'a', 'b'},

                    {'r', 'M', '2', 'i', 'F', '?', '.', '�', '�'},
                    {'q', 'N', '3', 'j', '=', 'E', '-', '�', '�'},
                    {'p', 'O', '4', 'k', ')', 'D', '/', '&', '%'},

                    {'o', 'P', 'l', '5', '6', '(', 'C', 'Z', '$'},
                    {'Q', 'n', 'm', 'U', '8', '7', 'Y', 'B', '�'},
                    {'R', 'S', 'T', '9', 'V', 'X', 'A', '!', '"'}
            };

    /**
     * Nur zum testen. Bitte String eingeben.
     */
    public static void main(final String[] args) {
        System.out.println(verschluesseln(args[0]));
        System.out.println(entschluesseln(verschluesseln(args[0])));
    }

    /**
     * Diese Methode kann Strings verschluesseln
     * Verschluesselt wird mit dem Feld "schluessel" dieser Klasse
     * Einzelne Zeichen werden dabei durch 2 ganze zahlen dagestellt
     * Der ausgabe String enthaelt nicht nur die verschluesselten Ganzzahlen,
     * sondern auch eine Pruefziffer (wichtig fuers entschluesseln)
     */
    public static String verschluesseln(final String eingabe) {
        final char[] chars = eingabe.toCharArray();
        String ausgabe = "";
        boolean gefunden = false; //bei doppelten Eintraegen im SchluesselArray benoetigt
        double pruefsumme = 0;
        for (int u = 0; u < chars.length; u++) {
            for (int i = 0; i < schluessel.length; i++) {
                for (int j = 0; j < schluessel.length; j++) {
                    if (chars[u] == schluessel[i][j] && !gefunden) {
                        ausgabe = ausgabe + i + j;
                        pruefsumme = pruefsumme + i + j;
                        gefunden = true;
                    }
                }
            }
            //befindet sich ein Character nicht im schluessel Array
            //dann wird dem ausgabe String ein 'x' + den Character hinzugefuegt
            if (!gefunden) {
                ausgabe = ausgabe + 'x' + chars[u];
                pruefsumme = pruefsumme + 1;
            }
            gefunden = false;
        }
        //pruefsumme wird auf eine stelle reduziert und dem ausgabe String hinzugefuegt
        ausgabe = Math.round((pruefsumme / 10 - Math.floor(pruefsumme / 10)) * 10) + ausgabe;
        return ausgabe;
    }

    /**
     * Diese Methode entschluesselt zuvor verschluesselte Strings wieder
     * Dabei wird im Falle der richtigen Pruefziffer der entschluesselte String ausgegeben
     * sonst "null"
     */
    public static String entschluesseln(final String eingabe) {
        final char[] chars = eingabe.toCharArray();
        String ausgabe = "";
        double pruefsumme = 0;
        try {
            for (int u = 1; u < chars.length - 1; u = u + 2) {
                if (chars[u] != 'x') {
                    final int i = Integer.valueOf(chars[u] + "");
                    final int j = Integer.valueOf(chars[u + 1] + "");
                    pruefsumme = pruefsumme + i + j;
                    ausgabe = ausgabe + schluessel[i][j];
                } else {
                    ausgabe = ausgabe + chars[u + 1];
                    pruefsumme = pruefsumme + 1;
                }
            }
            //pruefsumme wird ueberprueft //bei keiner uebereinstimmung "ausgabe=null"
            if (Math.round((pruefsumme / 10 - Math.floor(pruefsumme / 10)) * 10) == Double.valueOf(chars[0] + "")) {
                //tue nichts
            } else
                ausgabe = null;
        } catch (final IndexOutOfBoundsException e) {
            ausgabe = null;
        } catch (final NumberFormatException e) {
            ausgabe = null;
        }
        return ausgabe;
    }

}
