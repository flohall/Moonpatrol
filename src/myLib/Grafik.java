package myLib;

import java.awt.Color;
import java.awt.Graphics;

/**
 * Klasse Grafik
 * Diese Klasse stellt Methoden zur Verf�gung
 * die einfacheres Zeichnen erm�glichen.
 * Der Klasse muss zum Zeichnen
 * das Grafikobjekt einer Komponente �bergeben werden.
 * Entweder als Parameter in der genutzten Zeichenmethode
 * oder mit der Methode setzeGrafikObjekt(Graphics g)
 *
 * @author Florian Hallay
 * @version 19.10.2012
 */
public class Grafik {

    private static Graphics grafikObjekt;

    public static void setzeGrafikObjekt(final Graphics g) {
        grafikObjekt = g;
    }

    public static void loescheGrafikObjekt() {
        grafikObjekt = null;
    }

    public static void zeichneKreis(final int xMittelpunkt, final int yMittelpunkt, final int radius, final Graphics g) {
        g.drawOval(xMittelpunkt - radius, yMittelpunkt - radius, radius * 2, radius * 2);
    }

    public static void fuelleKreis(final int xMittelpunkt, final int yMittelpunkt, final int radius, final Graphics g) {
        g.fillOval(xMittelpunkt - radius, yMittelpunkt - radius, radius * 2, radius * 2);
    }

    public static void zeichneKreis(final Punkt mittelPunkt, final int radius, final Graphics g) {
        g.drawOval(mittelPunkt.getIntX() - radius, mittelPunkt.getIntY() - radius, radius * 2, radius * 2);
    }

    public static void fuelleKreis(final Punkt mittelPunkt, final int radius, final Graphics g) {
        g.fillOval(mittelPunkt.getIntX() - radius, mittelPunkt.getIntY() - radius, radius * 2, radius * 2);
    }

    public static void zeichneKreis(final int xMittelpunkt, final int yMittelpunkt, final int radius) {
        if (grafikObjekt != null)
            grafikObjekt.drawOval(xMittelpunkt - radius, yMittelpunkt - radius, radius * 2, radius * 2);
    }

    public static void fuelleKreis(final int xMittelpunkt, final int yMittelpunkt, final int radius) {
        if (grafikObjekt != null)
            grafikObjekt.fillOval(xMittelpunkt - radius, yMittelpunkt - radius, radius * 2, radius * 2);
    }

    public static void zeichneKreis(final int xMittelpunkt, final int yMittelpunkt, final int radius, final Color farbe) {
        if (grafikObjekt != null) {
            grafikObjekt.setColor(farbe);
            grafikObjekt.drawOval(xMittelpunkt - radius, yMittelpunkt - radius, radius * 2, radius * 2);
        }
    }

    public static void fuelleKreis(final int xMittelpunkt, final int yMittelpunkt, final int radius, final Color farbe) {
        if (grafikObjekt != null) {
            grafikObjekt.setColor(farbe);
            grafikObjekt.fillOval(xMittelpunkt - radius, yMittelpunkt - radius, radius * 2, radius * 2);
        }
    }

    public static void zeichneKreis(final Punkt mittelPunkt, final int radius, final Color farbe) {
        if (grafikObjekt != null) {
            grafikObjekt.setColor(farbe);
            final int x = mittelPunkt.getIntX() - radius;
            final int y = mittelPunkt.getIntY() - radius;
            grafikObjekt.drawOval(x, y, radius * 2, radius * 2);
        }
    }

    public static void fuelleKreis(final Punkt mittelPunkt, final int radius, final Color farbe) {
        if (grafikObjekt != null) {
            grafikObjekt.setColor(farbe);
            final int x = mittelPunkt.getIntX() - radius;
            final int y = mittelPunkt.getIntY() - radius;
            grafikObjekt.fillOval(x, y, radius * 2, radius * 2);
        }
    }

}
