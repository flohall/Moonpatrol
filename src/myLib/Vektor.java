package myLib;

/**
 * Klasse Vektor
 *
 * @author Florian Hallay
 * @version 19.10.2012
 */
public class Vektor {

    public final double x;

    public final double y;

    public Vektor() {
        x = 0;
        y = 0;
    }

    public Vektor(final double x, final double y) {
        this.x = x;
        this.y = y;
    }

    public Vektor(final Punkt p1, final Punkt p2) {
        x = p2.x - p1.x;
        y = p2.y - p1.y;
    }

    public int getIntX() {
        return (int) x;
    }

    public int getIntY() {
        return (int) y;
    }

    public double steigung() {
        return y / x;
    }

    public double betrag() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }

    public Vektor einheitsvektor() {
        final double betrag = betrag();
        return new Vektor(x / betrag, y / betrag);
    }

    public Vektor orthogonal() {
        return new Vektor(-y, x);
    }

}
