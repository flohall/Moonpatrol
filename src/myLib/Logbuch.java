package myLib;

import java.util.ArrayList;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * Klasse Logbuch:
 * Eigene Logging Klasse verwendet java.util.logging.*
 * Meldungen mit diesem Logger werden ueber die Konsole ausgegeben
 * mit getAusgabe() kann direkt auf die Ausgabe zugegriffen werden
 *
 * @author Florian Hallay
 * @version 28.12.2012
 */
public class Logbuch {

    private static LogHandler handler;

    private static boolean globalLoggerErzeugt;

    private static final Level loggerLevel = Level.FINER;

    /**
     * erzeugt einen globalen Logger
     */
    public static Logger getLogger() {
        final Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
        if (!globalLoggerErzeugt) {
            log.addHandler(LogHandler.getHandler());
            log.setLevel(loggerLevel);
        }
        globalLoggerErzeugt = true;
        return log;
    }

    public static Thread.UncaughtExceptionHandler getUncaughtEHandler() {
        return new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(final Thread t, final Throwable e) {
                final StackTraceElement ste = e.getStackTrace()[0];
                final String className = ste.getClassName();
                final String methodName = ste.getMethodName();
                endOfThread(className, methodName, e);
            }
        };
    }

    public static void finer(final String msg) {
        final StackTraceElement ste = Thread.currentThread().getStackTrace()[2];
        final String className = ste.getClassName();
        final String methodName = ste.getMethodName();
        getLogger().logp(Level.FINER, className, methodName, msg);
    }

    public static void info(final String msg) {
        final StackTraceElement ste = Thread.currentThread().getStackTrace()[2];
        final String className = ste.getClassName();
        final String methodName = ste.getMethodName();
        getLogger().logp(Level.INFO, className, methodName, msg);
    }

    public static void warning(final String msg) {
        final StackTraceElement ste = Thread.currentThread().getStackTrace()[2];
        final String className = ste.getClassName();
        final String methodName = ste.getMethodName();
        getLogger().logp(Level.WARNING, className, methodName, msg);
    }

    public static void severe(final String msg) {
        final StackTraceElement ste = Thread.currentThread().getStackTrace()[2];
        final String className = ste.getClassName();
        final String methodName = ste.getMethodName();
        getLogger().logp(Level.SEVERE, className, methodName, msg);
    }

    public static void throwing(final Throwable thrown) {
        final StackTraceElement ste = Thread.currentThread().getStackTrace()[2];
        final String className = ste.getClassName();
        final String methodName = ste.getMethodName();
        getLogger().logp(Level.FINE, className, methodName, "THROWN", thrown);
    }

    private static void endOfThread(final Throwable thrown) {
        final StackTraceElement ste = Thread.currentThread().getStackTrace()[2];
        final String className = ste.getClassName();
        final String methodName = ste.getMethodName();
        getLogger().logp(Level.SEVERE, className, methodName, "THROWN", thrown);
    }

    private static void endOfThread(final String className, final String methodName, final Throwable thrown) {
        getLogger().logp(Level.SEVERE, className, methodName, "THROWN", thrown);
    }

    public static ArrayList<String> getAusgabe() {
        return LogHandler.getAusgabe();
    }

    public static class LogHandler extends Handler {

        private static ArrayList<LogRecord> recordedLogs;

        private LogHandler() {
            recordedLogs = new ArrayList<LogRecord>();
            globalLoggerErzeugt = false;
        }

        /**
         * Logger fuer eine bestimmte Klasse
         * in der Regel besser getLogger() benutzen
         * Problem hier dem logger wird der handler mehrfach hinzugefuegt
         */
        @Deprecated
        public Logger createLogger(final Class cl) {
            final Logger log = Logger.getLogger(cl.getName());
            log.addHandler(getHandler());
            return log;
        }

        public static LogHandler getHandler() {
            if (handler == null)
                handler = new LogHandler();
            return handler;
        }

        @Override
        public void close() {
        }

        @Override
        public void flush() {
        }

        private static void logAufraeumen() {
            while (recordedLogs.size() >= 8) {
                int minLevel = recordedLogs.get(0).getLevel().intValue();
                int level = 0;
                for (int i = 1; i < recordedLogs.size(); i++) {
                    level = recordedLogs.get(i).getLevel().intValue();
                    if (minLevel > level)
                        minLevel = level;
                }
                for (int j = 0; j < recordedLogs.size(); j++) {
                    level = recordedLogs.get(j).getLevel().intValue();
                    if (minLevel == level) {
                        recordedLogs.remove(j);
                        break;
                    }
                }
            }
        }

        @Override
        public void publish(final LogRecord rec) {
            logAufraeumen();
            recordedLogs.add(rec);
        }

        public static ArrayList<String> getAusgabe() {
            final ArrayList<String> ausgabeString;
            ausgabeString = new ArrayList<String>();
            LogRecord rec;
            for (int i = 0; i < recordedLogs.size(); i++) {
                rec = recordedLogs.get(i);
                if (rec.getThrown() != null)
                    ausgabeString.add(rec.getLevel() + ": " + rec.getMessage() + " " + rec.getThrown());
                else
                    ausgabeString.add(rec.getLevel() + ": " + rec.getMessage());
                ausgabeString.add("..." + rec.getSourceClassName() + " " + rec.getSourceMethodName());
            }
            return ausgabeString;
        }

    }

}
