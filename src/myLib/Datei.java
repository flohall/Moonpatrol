package myLib;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.Component;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Klasse Datei:
 * ermoeglicht Zugriff auf verschiedene Dateitypen und liefert vollstaendige Pfade
 * die Dateien muessen sich im Ordner "../Dateien/" befinden
 * Bilder muessen in dem Unterordner "Bilder" befinden ("../Dateien/Bilder")
 *
 * @author Florian Hallay
 * @version 14.02.2013
 */
public class Datei {

    private static BufferedImage fehlerBild;

    private static Component observer;

    private static void erzeugeFehlerBild() {
        fehlerBild = new BufferedImage(80, 51, BufferedImage.TYPE_INT_ARGB);
        final java.awt.Graphics g = fehlerBild.createGraphics();
        g.setFont(g.getFont().deriveFont(20f));
        g.setColor(java.awt.Color.WHITE);
        g.fillRect(0, 0, 71, 51);
        g.setColor(java.awt.Color.RED);
        g.drawString("ERROR", 0, 15);
        g.drawString("ERROR", 0, 33);
        g.drawString("ERROR", 0, 51);
        g.dispose();
    }

    public static void schreibeTextdokument(final String dateiName, final String inhalt) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(dateiName);
            writer.write(inhalt);
        } catch (final IOException e1) {
            Logbuch.warning(dateiName + " konnte nicht geschrieben werden");
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (final IOException e3) {
                Logbuch.warning(dateiName + "FileWriter konnte nicht geschlossen werden");
            }
        }
    }

    public static String leseTextdokument(final String dateiName) {
        FileReader reader = null;
        String zeichenKette = "";
        java.util.Scanner scan = null;
        try {
            int charCode = 0;
            reader = new FileReader(dateiName);
            do {
                charCode = reader.read();
                if (charCode == -1)
                    break;
                zeichenKette = zeichenKette + (char) charCode;
            }
            while (charCode != -1);
            return zeichenKette;
        } catch (final FileNotFoundException e1) {
            //wenn die Datei sich in einem JAR-Archiv befindet
            //kann eine Application sie nicht mit dem FileReader finden
            //dann muss der Scanner benutzt werden
            try {
                scan = new java.util.Scanner(getRootInputStream(dateiName));
                while (scan.hasNextLine()) {
                    zeichenKette += scan.nextLine() + "\n\r";
                }
                return zeichenKette;
            } catch (final Exception e7) {
                if (getRootInputStream(dateiName) == null)
                    Logbuch.warning(dateiName + " kein Zugriff moeglich");
                else
                    Logbuch.throwing(e7);
                return null;
            }
        } catch (final IOException e2) {
            Logbuch.warning(dateiName + " konnte nicht geoeffnet werden");
            return null;
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (final IOException e4) {
                Logbuch.warning(dateiName + "FileReader konnte nicht geschlossen werden");
            }
        }
    }

    public static void loescheTextdokument(final String dateiName) {
        final File datei = new File(dateiName);
        if (datei.exists())
            datei.delete();
    }

    public static Icon leseIcon(final String pDateiname) {
        Icon ausgabeBild = null;
        final URL url = lieferePfad("bilder/" + pDateiname);
        try {
            ausgabeBild = new ImageIcon(url);
        } catch (final NullPointerException e) {
            Logbuch.warning(pDateiname + " nicht im Ordner 'bilder'");
        }
        return ausgabeBild;
    }

    /**
     * liest ein Bild aus dem Dateisystem und gibt es als Image zurueck
     * wenn das Bild nicht gefunden wurde, wird ein "Fehlerbild" (BufferedImage) zurueckgegeben
     */
    public static Image leseBild(final String pDateiname) {
        final Toolkit werkzeug = Toolkit.getDefaultToolkit();
        Image ausgabeBild = null;
        final URL url = lieferePfad("bilder/" + pDateiname);
        if (url == null) //wenn das Bild nicht gefunden wurde wird eine Fehlerbild uebergeben
        {
            if (fehlerBild == null)
                erzeugeFehlerBild();
            ausgabeBild = fehlerBild;
            Logbuch.warning(pDateiname + " nicht im Ordner 'bilder'");
        } else
            ausgabeBild = werkzeug.getImage(url);

        //das zeichnen der Bilder verz�gert sich zum Programmstart,
        //da sie erst beim zeichnen geladen werden
        //"Bild sofort laden" (in sekundaren Thread) kann mitunter dauern
        werkzeug.prepareImage(ausgabeBild, -1, -1, null);
        return ausgabeBild;
    }

    /**
     * wie leseBild nur mit ImageIO.read()
     */
    public static Image leseBildIO(final String pDateiname) {
        Image ausgabeBild = null;
        boolean fehler = false;
        final URL url = lieferePfad("bilder/" + pDateiname);

        try {
            ausgabeBild = javax.imageio.ImageIO.read(url);
        } catch (final IOException e1) {
            fehler = true;
            Logbuch.warning(pDateiname + " nicht im Ordner 'Bilder'");
        } catch (final java.lang.IllegalArgumentException e2) {
            fehler = true;
            Logbuch.warning(pDateiname + " nicht im Ordner 'Bilder'");
        }
        if (fehler || ausgabeBild == null) //bei einem Fehler wird ein Fehlerbild uebergeben
        {
            if (fehlerBild == null)
                erzeugeFehlerBild();
            ausgabeBild = fehlerBild;
        }
        return ausgabeBild;
    }

    /**
     * Ein Component, dient als ImageObserver
     */
    public static Component observer() {
        if (observer == null)
            observer = new Component() {
            };
        return observer;
    }

    /**
     * wartet bis das Bild vollstaendig geladen wurde
     * verwendet den MediaTracker
     */
    public static void warteAufBild(final Image bild) {
        if (bild != null) {
            final java.awt.MediaTracker tracker = new java.awt.MediaTracker(observer());
            tracker.addImage(bild, 0);
            try {
                tracker.waitForAll(); //warten bis das Bild geldaden wurde
            } catch (final InterruptedException e) {
            }
        }
    }

    /**
     * Diese Methode erstellt vollstaendige Pfade (nicht relativ)
     * Rootverzeichnis sind nicht die Pakete (wegen "/" am Anfang)
     * Pfad fuer Dateien im Ordner Dateien
     */
    public static URL lieferePfad(final String pUrl) {
        //getClass().getResource funktioniert nicht in statischen Metohden;
        return Datei.class.getResource("/" + pUrl);
    }

    /**
     * Diese Methode erstellt vollstaendige Pfade (nicht relativ)
     * Rootverzeichnis sind nicht die Pakete (wegen "/" am Anfang)
     * Pfad fuer Dateien im Rootverzeichnes des Projekts
     */
    public static URL liefereRootPfad(final String pUrl) {
        return Datei.class.getResource("/" + pUrl);
    }

    /**
     * InputStream fuer Dateien im Ordner Dateien
     */
    public static InputStream getInputStream(final String pUrl) {
        return Datei.class.getResourceAsStream("/" + pUrl);
    }

    /**
     * InputSream fuer Dateien im Rootverzeichnes des Projekts
     */
    public static InputStream getRootInputStream(final String pUrl) {
        return Datei.class.getResourceAsStream("/" + pUrl);
    }

}
