package myLib;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 * Klasse Verbindung
 * HTTP-URL Verbindungen aufbauen und benutzen
 *
 * @author Florian Hallay
 * @version 29.11.2012
 */
public class Verbindung {

    private final ArrayList<PostParam> postParams;

    private Verbindung() {
        postParams = new ArrayList<PostParam>();
    }

    public static Verbindung getVerbindung() {
        return new Verbindung();
    }

    public void addPostParam(final String name, final String wert) {
        postParams.add(new PostParam(name, wert));
    }

    public void sendeViaPost(final String urlString) {
        BufferedWriter writer = null;
        BufferedReader reader = null;
        try {
            String nachricht = "";
            for (int i = 0; i < postParams.size(); i++) {
                if (i > 0)
                    nachricht += "&";
                nachricht += postParams.get(i).toString();
            }
            final URL url = new URL(urlString);
            final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded;charset=UTF8");

            connection.setRequestProperty("Content-Length", String.valueOf(nachricht.length()));

            writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
            writer.write(nachricht);
            writer.flush();
            final String antwort = "";
            //es wird nichts gelesen //aber sonst funktioniert es nicht
            reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            // fuer Fehlermeldunen
            //                 for ( String line; (line = reader.readLine()) != null; )
            //             {
            //                 antwort+=line+"\n";
            //             }
            //             System.out.println(antwort);
            Logbuch.info("send <" + nachricht + "> ok!");
        } catch (final IOException e) {
            Logbuch.throwing(e);
        } catch (final Exception ex) {
            Logbuch.throwing(ex);
        } finally {
            try {
                writer.close();
                reader.close();
            } catch (final IOException e3) {
                Logbuch.throwing(e3);
            } catch (final NullPointerException e4) {
                //tue nichts
            }
        }
    }

    public static String empfangeVonUrl(final String urlString) {
        String antwort = "";
        BufferedReader reader = null;
        try {
            final URL url = new URL(urlString);
            final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            for (String line; (line = reader.readLine()) != null; ) {
                antwort += line + "\n";
            }
        } catch (final MalformedURLException e) {
            Logbuch.throwing(e);
        } catch (final IOException e2) {
            Logbuch.throwing(e2);
        } catch (final Exception ex) {
            Logbuch.throwing(ex);
        } finally {
            try {
                reader.close();
            } catch (final IOException e3) {
                Logbuch.throwing(e3);
            } catch (final NullPointerException e4) {
            }
        }
        if (antwort.isEmpty())
            antwort = null;
        return antwort;
    }

    private static class PostParam {

        private final String name;

        private final String wert;

        public PostParam(final String name, final String wert) {
            this.name = name;
            this.wert = URLEncoder.encode(wert, StandardCharsets.UTF_8);
        }

        public String toString() {
            return name + "=" + wert;
        }

    }

}
