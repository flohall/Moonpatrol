package myLib;

/**
 * Klasse zum messen von Zeit
 * Basis ist die funktion System.nanoTime()
 *
 * @author Florian Hallay
 * @version 19.10.2012
 */
public class Uhr {

    long zstart;

    // Konstruktor
    public Uhr() {
        zstart = 0;
    }

    public void starte() {
        try {
            zstart = System.nanoTime();
        } catch (final NumberFormatException e) {
            System.err.print(e);
            return;
        }
    }

    public double zeit() {
        try {
            return (System.nanoTime() - zstart) / 1000000;
        } catch (final NumberFormatException e) {
            System.err.print(e);
            return 0;
        }
    }

    public int nanos() {
        try {
            return (int) (System.nanoTime() - zstart);
        } catch (final NumberFormatException e) {
            System.err.print(e);
            return 0;
        }
    }

}
