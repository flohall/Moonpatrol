package myLib.sound;

import myLib.Datei;
import myLib.Logbuch;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequencer;
import java.io.IOException;
import java.io.InputStream;

/**
 * Eine Klasse zum Abspielen von Midifiles
 *
 * @author Florian Hallay
 * @version 19.01.2013
 */
public class MidiTon extends AbstraktTon {

    final String filename;

    Sequencer sequencer;

    /**
     * Ein neuer MidiTon wird erzeugt und geladen (nicht abgespielt)
     */
    public MidiTon(final String filename) {
        this.filename = filename;
        load();
    }

    /**
     * laden der Musikdatei (intern)
     */
    @Override
    protected void load() {
        try {
            sequencer = MidiSystem.getSequencer();
            sequencer.open();
            final InputStream midiFile = Datei.getInputStream("Musik/" + filename);
            sequencer.setSequence(MidiSystem.getSequence(midiFile));
        } catch (final MidiUnavailableException e) {
            Logbuch.throwing(e);
        } catch (final InvalidMidiDataException e2) {
            Logbuch.throwing(e2);
        } catch (final IOException e3) {
            Logbuch.throwing(e3);
        }
    }

    /**
     * Einmaliges abspielen
     */
    @Override
    public void play() {
        stop();
        if (sequencer != null) {
            sequencer.start();
        }
    }

    /**
     * Abspielen in einer Endlosschleife
     */
    @Override
    public void loop() {
        stop();
        if (sequencer != null) {
            sequencer.setLoopCount(Sequencer.LOOP_CONTINUOUSLY);
            sequencer.start();
        }
    }

    /**
     * Das Abspielen wird gestoppt
     */
    @Override
    public void stop() {
        if (sequencer != null) {
            sequencer.stop();
            sequencer.setTickPosition(0);
        }
    }

    /**
     * gibt zurueck, ob die Midifile gerade abgespielt wird
     */
    @Override
    public boolean laeuft() {
        boolean ausgabe = false;
        if (sequencer != null) {
            ausgabe = sequencer.isRunning();
        }
        return ausgabe;
    }

    /**
     * Destruktor
     */
    @Override
    public void close() {
        if (sequencer != null) {
            sequencer.stop();
            sequencer.close();
        }
    }

}
