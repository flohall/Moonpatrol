package myLib.sound;

/**
 * Abstract class AbstraktTon
 *
 * @author Florian Hallay
 * @version 28.12.2012
 */
public abstract class AbstraktTon {

    /**
     * laden der Musikdatei (intern)
     */
    protected abstract void load();

    /**
     * Einmaliges abspielen
     */
    public abstract void play();

    /**
     * Abspielen in einer Endlosschleife
     */
    public abstract void loop();

    /**
     * Das Abspielen wird gestoppt
     */
    public abstract void stop();

    /**
     * gibt zurueck, ob der Ton gerade abgespielt wird
     */
    public abstract boolean laeuft();

    /**
     * Destruktor
     */
    public abstract void close();

}


