package myLib.sound;

import myLib.Logbuch;

/**
 * Eine Klasse zum Abspielen verschiedener Musikdateiformate
 * unterstuetzt werden WAV und MIDI
 * Musikdateien muessen sich im Unterordner Musik befinden
 * ("../Dateien/Musik")
 *
 * @author Florian Hallay
 * @version 28.12.2012
 */
public class Ton {

    AbstraktTon ton;

    /**
     * Constructor for objects of class Ton
     */
    public Ton(final String filename, final boolean oneTimePlay) {
        if (filename.endsWith(".mid")) {
            if (!oneTimePlay)
                ton = new MidiTon(filename);
            else
                Logbuch.warning("oneTimePlay bei Midi nicht unterst�tzt");
        } else if (filename.endsWith(".wav"))
            ton = new WavTon(filename, oneTimePlay);
        else
            Logbuch.warning("unbekanntes Format [ " + filename + " ]");
    }

    /**
     * Einmaliges abspielen
     */
    public void play() {
        if (ton != null)
            ton.play();
    }

    /**
     * Abspielen in einer Endlosschleife
     */
    public void loop() {
        if (ton != null)
            ton.loop();
    }

    /**
     * Das Abspielen wird gestoppt
     */
    public void stop() {
        if (ton != null)
            ton.stop();
    }

    /**
     * gibt zurueck, ob die Midifile gerade abgespielt wird
     */
    public boolean laeuft() {
        if (ton != null)
            return ton.laeuft();
        else
            return false;
    }

    /**
     * Destruktor
     */
    public void close() {
        if (ton != null)
            ton.close();
    }

}
