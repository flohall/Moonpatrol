package myLib.sound;

import myLib.Datei;
import myLib.Logbuch;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;

/**
 * Klasse zum Abspielen von WAV-Dateien
 * Musikdateien muessen sich im Unterordner Musik befinden
 * ("../Dateien/Musik")
 *
 * @author Florian Hallay
 * @version 02.05.2013
 */
public class WavTon extends AbstraktTon {

    private final URL pfad;

    private final String filename;

    private Clip clip;

    /**
     * Ein neuer WavTon wird erzeugt (nicht abgespielt)
     * Er wird geladen, wenn preLoad true ist
     */
    public WavTon(final String filename, final boolean oneTimePlay) {
        pfad = Datei.lieferePfad("Musik/" + filename);
        this.filename = filename;
        if (oneTimePlay) {
            load();
            if (clip != null)
                clip.addLineListener(new MyLineListener());
        } else
            threadLoad();
    }

    /**
     * laden der Musikdatei (intern)
     */
    @Override
    protected void load() {
        AudioInputStream inputStream = null;
        clip = null;
        try {
            inputStream = AudioSystem.getAudioInputStream(pfad);
        } catch (final UnsupportedAudioFileException e) {
            Logbuch.throwing(e);
        } catch (final IOException e1) {
            Logbuch.throwing(e1);
        } catch (final NullPointerException e2) {
            Logbuch.warning(filename + " nicht im Ordner 'Musik'");
        }
        if (inputStream != null) {
            final BufferedInputStream buffStream = new BufferedInputStream(inputStream);
            final AudioFormat format = inputStream.getFormat();
            final int size = (int) (format.getFrameSize() * inputStream.getFrameLength());
            final byte[] bytes = new byte[size];
            final DataLine.Info info = new DataLine.Info(Clip.class, format, size);
            try {
                buffStream.read(bytes, 0, size);
            } catch (final IOException e3) {
                Logbuch.throwing(e3);
            }
            try {
                clip = (Clip) AudioSystem.getLine(info);
                clip.open(format, bytes, 0, size);
            } catch (final LineUnavailableException e4) {
                Logbuch.throwing(e4);
            }
        }
    }

    protected void playAndClose() {
        if (clip != null) {
            clip.addLineListener(new MyLineListener());
            play();
        }
    }

    private void threadLoad() {
        new Thread("WavTonLoad") {
            @Override
            public void run() {
                load();
            }
        }.start();
    }

    /**
     * Einmaliges abspielen
     */
    @Override
    public void play() {
        if (clip != null) {
            clip.setFramePosition(0);
            clip.start();
        }
    }

    /**
     * Abspielen in einer Endlosschleife
     */
    @Override
    public void loop() {
        if (clip != null) {
            clip.setFramePosition(0);
            clip.loop(Clip.LOOP_CONTINUOUSLY);
        }
    }

    /**
     * Das Abspielen wird gestoppt
     */
    @Override
    public void stop() {
        if (clip != null) {
            clip.stop();
        }
    }

    /**
     * gibt zurueck, ob der Ton gerade abgespielt wird
     */
    @Override
    public boolean laeuft() {
        boolean ausgabe = false;
        if (clip != null) {
            ausgabe = clip.isRunning();
        }
        return ausgabe;
    }

    /**
     * Destruktor
     */
    @Override
    public void close() {
        if (clip != null) {
            clip.stop();
            clip.close();
        }
    }

    class MyLineListener implements LineListener {

        @Override
        public void update(final LineEvent event) {
            if (event.getType() == LineEvent.Type.STOP)
                close();
        }

    }

}

